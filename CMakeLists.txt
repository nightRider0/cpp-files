cmake_minimum_required(VERSION 3.8)
project(cpp)

set(CMAKE_CXX_STANDARD 14)

set(SOURCE_FILES )


add_executable(tree_creation src/codeshsala/tree_creation.cpp)
set_target_properties(tree_creation PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/srcout")
add_executable(buildTree src/hackerearth/Ds/segmentTree/buildTree.cpp)
set_target_properties(buildTree PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/srcout")
add_executable(minimum src/hackerearth/Ds/segmentTree/minimum.cpp)
set_target_properties(minimum PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/srcout")
add_executable(intersectionPointLinkedList src/codeshsala/intersectionPointLinkedList.cpp)
set_target_properties(intersectionPointLinkedList PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/srcout")

add_executable(merge_link_list src/codeshsala/linked_list/merge_link_list.cpp)
set_target_properties(merge_link_list PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/srcout")
add_executable(palindrome_dllist src/codeshsala/linked_list/palindrome_dllist.cpp)
set_target_properties(palindrome_dllist PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/srcout")
add_executable(nth_last_node src/codeshsala/linked_list/nth_last_node.cpp)
set_target_properties(nth_last_node PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/srcout")
add_executable(swap src/codeshsala/linked_list/swap.cpp)
set_target_properties(swap PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/srcout")
add_executable(findLoop src/codeshsala/linked_list/findLoop.cpp)
set_target_properties(findLoop PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/srcout")
add_executable(DDA src/university/fifthsem/CGI/DDA.CPP)
set_target_properties(DDA PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/srcout")