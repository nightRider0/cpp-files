//
// Created by Akhilesh on 11-11-2017.
//
#include<bits/stdc++.h>

using namespace std;

map<long, long> cache;

long sol1(long n){
    if(n<1){
        return 0;
    }
    return max(n , sol1(n/2)+sol1(n/3)+sol1(n/4) );
}

long sol(long n){
    if(n<1){
        return 0;
    }
    if(cache[n] == 0 ){
        cache[n]= max(n , sol(n/2)+sol(n/3)+sol(n/4) );
    }
    return cache[n];
}

int main() {
    //long n;
    //cin>>n;
    for(long i = 0; i < 1000000000; i++) {
        long temp1 = sol1(i);
        long temp = sol(i);
        if(temp1 != temp){
            cout <<i << " :- " << temp << "  ::  " << temp1 <<endl;
        }
        cout <<  1000000000 - i << " remaining \n";
    }

}
