#include <bits/stdc++.h>
using namespace std;


bool sol(vector<int> &vec){
   int star = 0 ,end = vec.size()-1;
    int t =1;
    while(true){
        if(star>end && t == 7){
            return  true;
        }else if ((vec[star] == t )&&(vec[end] == t)){
            star++;
            end--;
        }else if((vec[star] == t+1 )&&(vec[end] == t+1)){
            star++;
            end--;
            t +=1 ;
        }else{
            return false;
        }
    }

}

int main(){
    int t ;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        vector <int> vec;
        for (int i = 0; i < n ; ++i) {
            int a;
            cin>>a;
            vec.push_back(a);
        }
        cout<< (sol(vec)?"yes\n":"no\n");

    }
}