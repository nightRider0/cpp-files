//
// Created by Akhilesh on 09-08-2017.
//
#include <bits/stdc++.h>
#include <w32api/wsman.h>

using namespace std;
int main(){
    int t;
    cin>>t;
    while(t--){
        long selected=0,salary=0;
        long n,m;
        cin>>n>>m;
        bool company[m];
        vector<pair<int,pair<int,int> > > company_detail;
        memset(company,0, sizeof(company));

        long min_salary[n];
        for(int i=0;i<n;i++){
            cin>>min_salary[i];
        }
        for (long i=0;i<m;i++){
            long mjo, os;
            cin>>os>>mjo;
            company_detail.emplace_back(make_pair(i,make_pair(os,mjo)));
        }

        sort(company_detail.begin(),company_detail.end(),[](const auto&lhs,const auto&rhs){
            return lhs.second.first > rhs.second.first;
        });

        bool qual[n][m];
        for(int i=0;i<n;i++){
            string s;
            cin>> s;
            for(int j=0;j<m;j++){
                qual[i][j]= s[j]-'0';
            }
        }
        for(int i=0;i<n;i++){
            vector<long> cache;
            for(int j=0;j<m;j++){
                if(qual[i][j]) cache.emplace_back(j);
            }
            if(cache.empty())continue;
            for( long j=0; j<company_detail.size();++j){
                if(company_detail[j].second.second==0){
                    continue;
                }
                if(binary_search(cache.begin(), cache.end(), company_detail[j].first)){
                    if(company_detail[j].second.first>= min_salary[i]){
                        company_detail[j].second.second-=1;
                        salary+=company_detail[j].second.first;
                        selected++;
                        company[company_detail[j].first]=true;
                        break;
                    }
                }
            }
        }
        long empty_hand=0;
        for(bool i:company){
            if(!i) empty_hand++;
        }
        cout << selected << " " << salary << " " << empty_hand << endl;
    }
}