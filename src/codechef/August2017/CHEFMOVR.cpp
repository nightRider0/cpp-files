//
// Created by Akhilesh on 05-08-2017.
//
#include <bits/stdc++.h>
using namespace std;
int main(){
    int t;
    cin>>t;
    while(t--) {
        vector<long> vec;
        long n;
        cin >> n;
        long long sum =0;
        for (long i = 0; i < n; ++i) {
            long a;
            cin>>a;
            sum+=a;
            vec.push_back(a);
        }
        if(sum % vec.size() == 0){
            long avg = sum/vec.size();
            long long moves =0 ;
            for (long i : vec){
                moves+=abs(avg-i);
            }
            cout<<moves/2<<endl;
        }else{
            cout<< -1 <<endl;
        }
    }
}
