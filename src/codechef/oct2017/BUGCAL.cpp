//
// Created by Akhilesh on 11-11-2017.
//
#include<bits/stdc++.h>

using namespace std;

int main() {
    int t;
    cin>>t;
    while(t--){
        int num1, num2, sol=0;
        cin>>num1>>num2;
        int i = 0;
        while(num1||num2){
            sol += (((num1%10)+(num2%10))%10)*(pow(10, i));
            i++;
            num1 /= 10;
            num2 /= 10;
        }
        cout<<sol<<endl;
    }
}