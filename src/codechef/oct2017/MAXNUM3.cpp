//
// Created by Akhilesh on 11-11-2017.
//
#include<bits/stdc++.h>

using namespace std;

char CHAR = '0';

bool check2(string &n ){
    return !((n[n.length()-1] - CHAR)%2);
}

bool check3 ( int sum){
    return !(sum%3);
}

string sol(string &n, int sum){
    if(check2(n) && check3(sum) ){
        return n;
    }
    for(int i = n.length()-1; i > -1 ; i--){
        if(i == n.length()-1){
            if( check3(sum - (n[i]-CHAR)) ){
                if(i>0){
                    if(!(n[i-1]-CHAR)%2) {
                        n.erase(i,1);
                        return n;
                    }else{ return "-1"; }
                }else{ return "0";}
            } else if(! check2(n)){
                return "-1";
            }
        }else{
            if(check3(sum - (n[i]-CHAR)) ){
                n.erase(i,1);
                return n;
            }
        }
    }
}

int main(){
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        int sum = 0;
        for(int i=n; i; i/=10){
            sum += i%10;
        }
        string ns = to_string(n);
        cout << sol(ns , sum) <<endl;
    }
}