//
// Created by Akhilesh on 09-11-2017.
//
#include<bits/stdc++.h>

using namespace std;

void sol(string s, int* a, int* b){
    int temp = 0, i=0;
    char c ;

    while(i<s.length() && s[i] == '.') i++;
    if(i == s.length()){ return;}

    c = s[i];
    c == 'A'?(*a=1):(*b=1);
    i++;

    for(; i< s.length(); i++){
        if(s[i] == '.'){
            temp++;
        }else if(s[i] == c){
            temp++;
            c=='A'?(*a += temp):(*b +=temp);
            temp=0;
        }else if(s[i] != c){
            c=='B'?(*a += 1):(*b += 1);
            temp = 0;
            c = s[i];
        }
    }
}

int main() {
    int t;
    cin>>t;
    while(t--) {
        string s;
        cin >> s;
        int aVill=0, bVill=0;
        sol(s, &aVill, &bVill);
        cout<< aVill << " " << bVill <<endl;
    }
}
