//
// Created by Akhilesh on 09-11-2017.
//
#include<bits/stdc++.h>

using namespace std;

int main() {
    int t;
    cin>>t;
    while(t--){
        int n, ls;
        cin >> n >> ls;
        int arr[n];
        for(int i =0; i<n; i++){
            cin>>arr[i];
        }

        int min = INT_MIN;
        int max = INT_MAX;
        bool sol = true;
        if(arr[n-1] != ls) sol = false;
        for(int i=0; i< n-1 ;i++){
            if(!sol) break;
            if(arr[i] < max && arr[i] > min ){
                if(arr[i+1] < arr[i]) max = arr[i];
                else min = arr[i];
            }else{
                sol = false;
            }
        }
        if(arr[n-1] > max || arr[n-1] < min) sol = false;
        
        cout << (sol?"YES":"NO") <<endl;
    }
}