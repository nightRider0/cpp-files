//
// Created by Akhilesh on 08-09-2017.
//

#include<bits/stdc++.h>

bool check(const int num[]){
    for(int i =0; i< 6 ;i++){
        if(num[i] < 1){
            return false;
        }
    }
    for (int i = 6; i<9 ; i++){
        if(num[i] <2){
            return false;
        }
    }
    return (num[9]>=1);
}
int main(){
    int t ;
    std::cin >> t;
    while (t--){

        int num[10];
        int charter[26];
        memset(num,0, sizeof(num));
        memset(charter, 0 , sizeof(charter));
        std::string s;
        std::cin>>s;
        int z = 0;
        for (auto a: s) {
            if(z>20){
                if(check(num)){
                    break;
                }
            }
            z++;
            if(num[a - '0']<2) {
                num[a - '0']++;
            }
        }
        if(num[6] > 0){
            num[6]--;
            for(int i = 5; i<10; i++){
                if(num[i]>0){
                    charter[i-5]++;
                }
            }
            num[6]++;
        }
        if(num[7] > 0){
            num[7]--;
            for(int i = 0; i<10; i++){
                if(num[i]>0){
                    charter[i+5]++;
                }
            }
            num[7]++;
        }
        if(num[8] > 0){
            num[8]--;
            for(int i = 0; i<10; i++){
                if(num[i]>0){
                    charter[i+15]++;
                }
            }
            num[8]++;
        }
        if(num[9]>0 && num[0]>0 ){
            charter[25] ++;
        }
        for (int i = 0; i < 25 ; i++) {
            if(charter[i]>0) {
                std::cout<< ((char)(i + 65));
            }
        }
        std::cout << std::endl;
    }
}

