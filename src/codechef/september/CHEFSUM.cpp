//
// Created by Akhilesh on 07-09-2017.
//
#include<bits/stdc++.h>
using namespace std;
int main(){
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        vector<int> arr(n), sol(n);
        for (int i=0;i<n;i++){
            cin >> arr[i];
        }
        int temp = 0 ;
        for(int i =0; i < n; i++){
            temp += arr[i];
            sol[i] = temp;
        }
        temp = 0;
        for(int i = n-1; i >=0; i--){
            temp += arr[i];
            sol[i] += temp;
        }
        auto itr = min_element(sol.begin(),sol.end());
        cout << itr - sol.begin() + 1 << endl;
    }
}