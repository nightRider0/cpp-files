//
// Created by Akhilesh on 09-09-2017.
//
#include<bits/stdc++.h>
using namespace std;
int main(){

    int t ;
    cin>>t;
    while(t--){
        int n,m;
        cin >> n >> m;
        struct ds{int type; int left; int right; vector<int> *temp = nullptr;};
        vector<int> arr(n, 0);
        vector<ds> cmd(m);
        for (int j = 0; j < m ; ++j) {
            int type, left, right;
            cin >> type >> left >> right;
            if (type == 1) {
                for(int i=left-1; i<right; i++){
                    arr[i] = (arr[i]+1)%1000000007;
                }
                cmd[j].type = type;
                cmd[j].left = left;
                cmd[j].right = right;
                cmd[j].temp = nullptr;

            } else if (type == 2) {
                cmd[j].temp = new vector<int> ;
                for(auto el: arr){
                    cmd[j].temp ->emplace_back(el);
                }
                for(int i = left-1; i<right; i++){
                    int typ = cmd[i].type;
                    if(typ == 1){
                        for(int k=cmd[i].left-1; k<cmd[i].right; k++){
                            arr[k] = arr[k] %1000000007 ;
                        }
                    }else if(typ == 2){
                        for(int l = 0; l<n; l++){
                            arr[l] = arr[l]+cmd[i].temp->at(l);
                        }
                    }
                }
                cmd[j].type = type;
                cmd[j].left = left;
                cmd[j].right = right;
            }
        }
        for(auto ele : arr){
            cout << ele << " ";
        }
        cout<<endl;
    }

}