//
// Created by Akhilesh on 07-09-2017.
//
#include <bits/stdc++.h>
using namespace std;
bool CAPS = false;
bool ALPHA = false;
bool MOV = false;
bool DEL = false;
bool CNGRW = false;

char charOpr(char s){
    if(CAPS){
        if( s >= 'a' & s <= 'z'){
            return s - 'a' + 'A';
        }else return s;
    }else{
        return s;
    }
}

int opt(char s){
    if(ALPHA){
        return 2;
    }
    if(s == '@' ){
        CAPS = !CAPS;
        return 2;
    }else if(s == '#'){
        return 7;
    }else if(s == '<'){
        MOV = true;
        return -1;
    }else if(s == '>'){
        MOV = true;
        return 1;
    }else if(s == '/'){
        DEL = true;
        return -1;
    }else if(s == '?'){
        CNGRW = true;
        return 1;
    }else if(s == '^'){
        CNGRW = true;
        return -1;
    }
}

int checkALPHA(string s, int i){
    int temp = i;
    string c = "alpha numeric space";
    int j = 0;
    while(j<c.length() && i < s.length()){
        if(s[i] == c [j]){
            i++;
            j++;
        }else {return temp;}
    }
    ALPHA = !ALPHA;
    return i;
}

int main() {
    string input;
    getline(cin, input);
    vector<char> scr;


    int row = 0;
    int col = 0;

    for(int i =0 ; i<input.length(); i++ ){
        int value;
        i = checkALPHA(input, i);
        int TEMP = 0;
        value = opt(input[i]);
        if(value == 2) continue;

        if(value == 1 && CNGRW){
            int j=0;
            while(CNGRW){
                TEMP+=value;
                CNGRW = false;
                j++;
                value = opt(input[i+j]);
            }

            j--;
            i+=j;
            value = TEMP<0?-1:1;
            CNGRW = true;
        }

        if(!value){
            char temp = charOpr(input[i]);
            if(col == scr.size()){
                scr.emplace_back(temp);
                col++;
            }else{
                scr.insert(scr.begin()+col, temp);
                col++;
            }
        }else{
            if(value == 7){
                if(col == scr.size()){
                    scr.emplace_back('\n');
                    col++;
                }else {
                    scr.insert(scr.begin() + col, '\n');
                    col++;
                }
            }
            else if(value == 1){
                if(MOV){
                    col++;
                    if(col > scr.size()){
                        col = (int)scr.size();
                    }
                    MOV = false;
                }else if (CNGRW){
                    int temp = col;
                    while(TEMP--) {
                        while (scr[col] != '\n') {
                            col++;
                        }
                        col++;
                    }
                    while(col < col+temp) {
                        if (scr[col] == '\n') {
                            break;
                        }
                        col++;
                    }
                    if(col>scr.size())
                    CNGRW = false;
                }

            }
            else if (value == -1){
                if(MOV){
                    col--;
                    if(col < 0){
                        col = 0;
                    }
                }else if (CNGRW){
                    int temp = col;
                    while(scr[col]!='\n'){
                        col--;
                    }
                    while(TEMP--) {
                        while (scr[col] != '\n') {
                            col--;
                        }
                        col--;
                    }
                    while(col < col+temp) {
                        if (scr[col] == '\n') {
                            break;
                        }
                        col++;
                    }

                    CNGRW = false;
                }else if(DEL){
                    scr.erase(scr.begin()+col);
                }

            }
        }
    }

    for(auto it : scr){
            cout << it;
    }

}
