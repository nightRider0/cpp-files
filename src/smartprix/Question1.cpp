#include <bits/stdc++.h>
using namespace std;
bool CAPS = false;
bool ALPHA = false;
bool MOV = false;
bool DEL = false;
bool CNGRW = false;

char charOpr(char s){
    if(CAPS){
        if( s >= 'a' & s <= 'z'){
            return s - 32;
        }else return s;
    }else{
        return s;
    }
}

int opt(char s){
    if(ALPHA){
        return 2;
    }
    if(s == '@' ){
        CAPS = !CAPS;
        return 2;
    }else if(s == '#'){
        return 7;
    }else if(s == '<'){
        MOV = true;
        return -1;
    }else if(s == '>'){
        MOV = true;
        return 1;
    }else if(s == '/'){
        DEL = true;
        return -1;
    }else if(s == '?'){
        CNGRW = true;
        return 1;
    }else if(s == '^'){
        CNGRW = true;
        return -1;
    }
}

int checkALPHA(string s, int i){
    int temp = i;
    string c = "alpha numeric space";
    int j = 0;
    while(j<c.length() && i < s.length()){
        if(s[i] == c [j]){
            i++;
            j++;
        }else {return temp;}
    }
    ALPHA = !ALPHA;
    return i;
}

int main() {
    string input;
    getline(cin, input);
    vector < vector < char > > scr(100);

    vector < bool > rowVisit(100);
    for ( auto it : rowVisit ){
        it = false;
    }
    vector < int > colCount(100);
    for ( auto it: colCount ){
        it = 0;
    }

    int row = 0;
    int col = 0;

    for(int i =0 ; i<input.length(); i++ ){
        int value;
        i = checkALPHA(input, i);
        int TEMP = 0;
        value = opt(input[i]);
        if(value == 2) continue;
        if(value == 1 && CNGRW){
            int j=0;
            while(CNGRW){
                TEMP+=value;
                CNGRW = false;
                j++;
                value = opt(input[i+j]);
            }

            j--;
            i+=j;
            value = TEMP<0?-1:1;
            CNGRW = true;
        }

        if(!value){
            char temp = charOpr(input[i]);
            if(col >= colCount[row]){
                scr[row].emplace_back(temp);
                colCount[row]++;
                col++;
            }else if(col < colCount[row]){
                scr[row].insert(scr[row].begin()+row, temp);
                colCount[row]++;
                col++;
            }
        }else{
            if(value == 7){
                if(col == 0 || col > colCount[row]){
                    scr[row].emplace_back('\n');
                    row++;
                    rowVisit[row] =true;
                }else if(col < colCount[row]){
                    scr[row].insert(scr[row].begin()+col, '\n');
                    //scr.insert(scr.begin()+(row+1), new vector<int>);
                    col++;
                    /*auto it;
                    for(it = scr[row].begin()+col; it != scr[row].end(); it++){
                        scr[row+1].emplace_back(it);
                    }
                    colCount[row] = col;
                    colCount[row+1] = scr[row+1].size();

                    row++;
                    colCount.insert(colCount.begin()+row,0);*/
                    col = colCount[row];
                }
            }
            else if(value == 1){
                if(MOV){
                    if(col == colCount[row]){
                        row++;
                        col=0;
                    } else if (col <colCount[row]){
                        col++;
                    }
                    MOV = false;
                }else if (CNGRW){
                    row = row - TEMP;
                    if(row < 0){
                        row = 0;
                    }
                    if(col < colCount[row]){
                        col = colCount[row];
                    }
                    CNGRW = false;
                }

            }
            else if (value == -1){
                if(MOV){
                    if(col >0){
                        col--;;
                    } else if (col == 0){
                        row -- ;
                        col = colCount[row];
                    }
                }else if (CNGRW){
                    while(TEMP){
                        row = row+1;
                        if(!rowVisit[row]){
                            break;
                        }
                        TEMP--;
                    }
                    if(col <= colCount[row]){
                        col = colCount[row];
                    }
                }else if(DEL){
                    if(col >0){
                        scr[row].erase(scr[row].begin()+col);
                        col--;
                    } else if (col == 0){
                        row -- ;
                        rowVisit[row+1] = false;
                        col = colCount[row];
                    }
                }

            }
        }
    }

    for(auto it : scr){
        for(auto i : it){
            cout<<i;
        }
    }

}
