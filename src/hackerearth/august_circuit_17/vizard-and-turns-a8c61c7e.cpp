//
// Created by Akhilesh on 22-08-2017.
//
//debug it
#include <bits/stdc++.h>
using namespace std;
int main() {
    int n, m;
    cin >> n >> m;
    char arr[n][m];
    pair<int, int> v, h;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            cin >> arr[i][j];
            if (arr[i][j] == 'V') {
                v.first = i;
                v.second = j;
            }
            if (arr[i][j] == 'H') {
                h.first = i;
                h.second = j;
            }
        }
    }
    int dp[n][m];
    int px[n][m] , py[n][m];
    for(int i=0;i<n;i++){
        for (int j = 0; j <m ; ++j) {
            dp[i][j] = INT_MAX;
            px[i][j] = INT_MAX;
            py[i][j] = INT_MAX;
        }
    }
    queue<pair<int, int> > q;
    q.push(make_pair(v.first, v.second));
    dp[v.first][v.second] = 0;
    px[v.first][v.second] = v.first;
    py[v.first][v.second] = v.second;
    while (!q.empty()) {
        pair<int, int> temp;
        temp = q.front();
        q.pop();
        if (temp.first == h.first && temp.second == h.second)continue;

        if (temp.first + 1 < n && (arr[temp.first + 1][temp.second] == '.' || temp.first + 1 < m && arr[temp.first + 1][temp.second] == 'H')) {
            if (py[temp.first][temp.second] - temp.second == 0) {
                if (dp[temp.first + 1][temp.second] > dp[temp.first][temp.second]) {
                    dp[temp.first + 1][temp.second] = dp[temp.first][temp.second];
                    px[temp.first + 1][temp.second] = temp.first;
                    py[temp.first + 1][temp.second] = temp.second;
                    q.push(make_pair(temp.first + 1, temp.second));
                }
            } else {
                if (dp[temp.first + 1][temp.second] > dp[temp.first][temp.second] + 1) {
                    dp[temp.first + 1][temp.second] = dp[temp.first][temp.second] + 1;
                    px[temp.first + 1][temp.second] = temp.first;
                    py[temp.first + 1][temp.second] = temp.second;
                    q.push(make_pair(temp.first + 1, temp.second));
                }
            }
        }
        if (temp.second + 1 < m && (arr[temp.first][temp.second + 1] == '.' || temp.second + 1 < n && arr[temp.first][temp.second + 1] == 'H')) {
            if (px[temp.first][temp.second] - temp.first == 0) {
                if (dp[temp.first][temp.second + 1] > dp[temp.first][temp.second]) {
                    dp[temp.first][temp.second + 1] = dp[temp.first][temp.second];
                    px[temp.first][temp.second + 1] = temp.first;
                    py[temp.first][temp.second + 1] = temp.second;
                    q.push(make_pair(temp.first, temp.second + 1));
                }
            } else {
                if (dp[temp.first][temp.second + 1] > dp[temp.first][temp.second] + 1) {
                    dp[temp.first][temp.second + 1] = dp[temp.first][temp.second] + 1;
                    px[temp.first][temp.second + 1] = temp.first;
                    py[temp.first][temp.second + 1] = temp.second;
                    q.push(make_pair(temp.first, temp.second + 1));
                }
            }
        }
        if (temp.first - 1 > 0 && (arr[temp.first - 1][temp.second] == '.' || temp.first - 1 > 0 && arr[temp.first - 1][temp.second] == 'H')) {
            if (py[temp.first][temp.second] - temp.second == 0) {
                if (dp[temp.first - 1][temp.second] > dp[temp.first][temp.second]) {
                    dp[temp.first - 1][temp.second] = dp[temp.first][temp.second];
                    px[temp.first - 1][temp.second] = temp.first;
                    py[temp.first - 1][temp.second] = temp.second;
                    q.push(make_pair(temp.first - 1, temp.second));
                }
            } else {
                if (dp[temp.first - 1][temp.second] > dp[temp.first][temp.second] + 1) {
                    dp[temp.first - 1][temp.second] = dp[temp.first][temp.second] + 1;
                    px[temp.first - 1][temp.second] = temp.first;
                    py[temp.first - 1][temp.second] = temp.second;
                    q.push(make_pair(temp.first - 1, temp.second));
                }
            }
        }
        if (temp.second - 1 > 0 && (arr[temp.first][temp.second - 1] == '.' || temp.second - 1 > 0 && arr[temp.first][temp.second - 1] == 'H')) {
            if (px[temp.first][temp.second] - temp.first == 0) {
                if (dp[temp.first][temp.second - 1] > dp[temp.first][temp.second]) {
                    dp[temp.first][temp.second - 1] = dp[temp.first][temp.second];
                    px[temp.first][temp.second - 1] = temp.first;
                    py[temp.first][temp.second - 1] = temp.second;
                    q.push(make_pair(temp.first, temp.second - 1));
                }
            } else {
                if (dp[temp.first][temp.second - 1] > dp[temp.first][temp.second] + 1) {
                    dp[temp.first][temp.second - 1] = dp[temp.first][temp.second] + 1;
                    px[temp.first][temp.second - 1] = temp.first;
                    py[temp.first][temp.second - 1] = temp.second;
                    q.push(make_pair(temp.first, temp.second - 1));
                }
            }
        }
    }
    cout << dp[h.first][h.second] << endl;
}
