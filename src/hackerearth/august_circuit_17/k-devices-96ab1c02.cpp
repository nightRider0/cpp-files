//
// Created by Akhilesh on 22-08-2017.
//
#include <bits/stdc++.h>
using namespace std;

long fsqrt(long sq) {
    for (long i = 0; i < sq; i++) {
        if (i * i >= sq)return i;
    }
}

long distance(int x, int y) {
    int sq = x * x + y * y;
    return fsqrt(sq);
}

int main() {
    int n, k;
    cin >> n >> k;
    vector<int> x, y;
    for (int i = 0; i < n; i++) {
        int temp;
        cin >> temp;
        x.push_back(temp);
    }

    for (int i = 0; i < n; i++) {
        int temp;
        cin >> temp;
        y.push_back(temp);
    }

    vector<long> dis;
    for (int i = 0; i < n; i++) {
        dis.emplace_back(distance(x[i], y[i]));
    }
    sort(dis.begin(), dis.end());

    cout << dis[k - 1] << endl;
    return 0;
}