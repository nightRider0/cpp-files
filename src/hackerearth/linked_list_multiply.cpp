//
// Created by Akhilesh on 19-08-2017.
//
#include <bits/stdc++.h>
using namespace std;
class Node{
private:
    int data;
    Node* next;
public:
    explicit Node(int _data = -1){
        this->data = _data;
        this->next = nullptr;
    }
    int getData() const {
        return data;
    }

    void setData(int data) {
        Node::data = data;
    }

    Node*getNext() const {
        return next;
    }

    void setNext( Node *next) {
        Node::next = next;
    }
};
class r_linked_list{
private:
    Node *head = nullptr;
    Node *current = nullptr;
    int size = 0;
    //bool head_reset(Node * temp) {head = temp;}
    Node * create_Node(int data, Node* next = nullptr){
        auto *temp = new Node(data);
        temp->setNext(next);
        return temp;
    }
    deque<Node*> vec ;
public:
    explicit r_linked_list(Node *head = nullptr) : head(head) {current = head;}

    void current_reset(){current = head;}

    bool insert_value(int data){
        if (head == nullptr) {
            head=create_Node(data);
            vec.push_front(head);
            current_reset();
            size++;
        } else {
            head = create_Node(data,head);
            vec.push_front(current);
            size++;
        }
        return true;
    }

    /*bool add(int data){
        if(current!= nullptr)
        current->setData(data);
        else if(vec.empty()){
            insert_value(data);
        }
        else {
            current = head;
            current->setNext(create_Node(data));
            current = current->getNext();
            vec.push_back(current);
            size++;
        }
    }*/

    int length(){return this->size;};

    int get_value(){
        if(current == nullptr){ return 0;}
        int temp =  current->getData();
        return temp;
    }

    void next(){current = current->getNext();}

    bool goto_index(int index){
        if(index<0) return false;
        if(vec.empty()) {
            current = nullptr;
            return false;
        }
        if(index > size-1)return false;
        current = vec[index];
        return true;
    }

    virtual ~r_linked_list() {
        while(head!= nullptr){
            Node* temp = head ;
            head = head->getNext();
            delete temp;
        }
    }
};

class linked_list{
private:
    Node *head = nullptr;
    Node *current = nullptr;
    int size = 0;
    //bool head_reset(Node * temp) {head = temp;}
    Node * create_Node(int data, Node* next = nullptr){
        auto *temp = new Node(data);
        temp->setNext(next);
        return temp;
    }
    vector<Node*> vec ;
public:
    explicit linked_list(Node *head = nullptr) : head(head) {current = head;}

    void current_reset(){current = head;}

    bool insert_value(int data){
        if (head == nullptr) {
            head=create_Node(data);
            vec.push_back(head);
            current_reset();
            size++;
        } else {
            current->setNext(create_Node(data));
            current = current->getNext();
            vec.push_back(current);
            size++;
        }
        return true;
    }

    bool add(int data){
        if(current!= nullptr)
            current->setData(data);
        else if(vec.empty()){
            insert_value(data);
        }
        else {
            current = vec.back();
            current->setNext(create_Node(data));
            current = current->getNext();
            vec.push_back(current);
            size++;
        }
    }

    int length(){return this->size;};

    int get_value(){
        if(current == nullptr){ return 0;}
        int temp =  current->getData();
        return temp;
    }

    void next(){current = current->getNext();}

    bool goto_index(int index){
        if(index<0) return false;
        if(vec.empty()) {
            current = nullptr;
            return false;
        }
        if(index > size-1)return false;
        current = vec[index];
        return true;
    }

    virtual ~linked_list() {
        while(head!= nullptr){
            Node* temp = head ;
            head = head->getNext();
            delete temp;
        }
    }
};


int main(){
    auto list1 = new r_linked_list, list2 = new r_linked_list;
    int n1, n2 ;
    cin>> n1;
    for(int i=0; i<n1 ;i++){
        int temp ;
        cin>>temp;
        list1->insert_value(temp);
    }
    cin >> n2;
    for(int i=0; i<n2 ;i++){
        int temp ;
        cin>>temp;
        list2->insert_value(temp);
    }
    list2->current_reset();


    auto list3 = new linked_list();
    int k =0;
    list1->current_reset();
    list2->current_reset();
    list3->current_reset();
    int carry = 0;
    for (int j = 0; j < n2 ; ++j) {
        list3->goto_index(k);
        for (int i = 0; i < n1; ++i) {
            int temp = (list2->get_value() * list1->get_value()) + list3->get_value()+carry;
            list3->add(temp%10);
            carry = temp/10;
            list1->next();
            list3->next();
        }
        while(carry){
            list3->add(carry%10);
            carry /= 10;
            list3->next();
        }
        list1->current_reset();
        list2->next();
        k++;
    }

    list3->current_reset();
    stack<int> st;
    for(int i=0; i < list3->length(); i++){
        st.push(list3->get_value());
        list3->next();
    }
    for(int i=0; i< list3->length() ; ++i ){
        cout<<st.top()<<" ";
        st.pop();
    }
    cout<<endl;
}
