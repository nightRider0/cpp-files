
//
// Created by Akhilesh on 31-08-2017.
//
#include<bits/stdc++.h>
using namespace std;

string itos(long long n){
    stringstream ss;
    ss<<n;
    return ss.str();
}

string binary(long long n) {
    stringstream sol;
    long long t = 1;
    long long i = 0;
    while (n) {
        long long temp = n & t;
        if (temp) {
            sol << i;
        }
        n = n >> 1;
        i++;
    }
    string ss = sol.str();
    reverse(ss.begin(), ss.end());
    return ss;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        long long a,b;
        cin >> a >> b;
        string r = binary(b);
        for (int i = 0; i < r.length() ; ++i) {
            cout << "(" << a << "<<" << r[i] << ")";
            if(i!=r.length()-1){
                cout<< " + ";
            }
        }
        cout<<endl;
    }

}