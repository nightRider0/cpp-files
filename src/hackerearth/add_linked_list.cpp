//
// Created by Akhilesh on 22-08-2017.
//
#include<bits/stdc++.h>
using namespace std;

class Node{
private:
    int data;
    Node* next;
public:
    explicit Node(int _data = -1){
        this->data = _data;
        this->next = nullptr;
    }
    int getData() const {
        return data;
    }

    void setData(int data) {
        Node::data = data;
    }

    Node*getNext() const {
        return next;
    }

    void setNext( Node *next) {
        Node::next = next;
    }
};
class linked_list{
private:
    Node *head = nullptr;
    Node *current = nullptr;
    int size = 0;
    //bool head_reset(Node * temp) {head = temp;}
    Node * create_Node(int data, Node* next = nullptr){
        auto *temp = new Node(data);
        temp->setNext(next);
        return temp;
    }
    vector<Node*> vec ;
public:
    explicit linked_list(Node *head = nullptr) : head(head) {current = head;}

    void current_reset(){current = head;}

    bool insert_value(int data){
        if (head == nullptr) {
            head=create_Node(data);
            vec.push_back(head);
            current_reset();
            size++;
        } else {
            current->setNext(create_Node(data));
            current = current->getNext();
            vec.push_back(current);
            size++;
        }
        return true;
    }

    bool add(int data){
        if(current!= nullptr)
            current->setData(data);
        else if(vec.empty()){
            insert_value(data);
        }
        else {
            current = vec.back();
            current->setNext(create_Node(data));
            current = current->getNext();
            vec.push_back(current);
            size++;
        }
    }

    int length(){return this->size;};

    int get_value(){
        if(current == nullptr){ return 0;}
        int temp =  current->getData();
        return temp;
    }

    void next(){current = current->getNext();}

    bool goto_index(int index){
        if(index<0) return false;
        if(vec.empty()) {
            current = nullptr;
            return false;
        }
        if(index > size-1)return false;
        current = vec[index];
        return true;
    }

    virtual ~linked_list() {
        while(head!= nullptr){
            Node* temp = head ;
            head = head->getNext();
            delete temp;
        }
    }
};
int main(){

}
