//
// Created by Akhilesh on 14-09-2017.
//
#include <bits/stdc++.h>
using namespace std;

struct Node{
    int popularity;
    Node * next = nullptr;
};

Node *createList(int n){
    Node * mov = nullptr, *head = nullptr;
    while(n--){
        int t;
        cin>>t;
        if(head == nullptr){
            mov = new Node;
            mov->popularity = t;
            head = mov;
        }else{
            mov->next = new Node;
            mov = mov->next;
            mov->popularity = t;
        }
    }
    return head;
}

Node *sol(Node *head, int k, int n){
    bool deletefrient = false;
    while(true){
    Node * temp = head;
    Node * temp1 = head;
    int t = n;
    for(int i=1;i<n && k;i++){
        if(temp == head){
            if(temp->popularity < temp->next->popularity){
                head = temp->next;
                temp = head;
                temp1 = head;
                k--;
                t--;
            }else{
                temp1 = temp;
                temp = temp->next;
            }
        }
        else if(temp->popularity < temp->next->popularity){
            temp1->next = temp->next;
            temp = temp1->next;
            k--;
            t--;
        }else{
            temp = temp->next;
            temp1 = temp1->next;
        }
    }

    if(k>0){
        head = head->next;
        k--;
    }
    return head;
}

int main()
{
    int t;
    cin>>t;
    while(t--){
        int n;
        cin >> n;
        int k;
        cin>>k;
        Node *head;
        head = createList(n);
        head = sol(head, k, n);
        while(head!= nullptr){
            cout<< head->popularity <<" ";
            head = head->next;
        }
        cout<<endl;
    }
}

