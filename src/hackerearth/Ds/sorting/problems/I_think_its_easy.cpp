//
// Created by Akhilesh on 13-09-2017.
//
#include<bits/stdc++.h>
using namespace std;

int partition(vector<string> &arr, int start, int end){
    string piv = arr[start];
    int i =start+1;
    for(int  j=start+1; j<=end; j++){
        if(arr[j].length()<piv.length()){
            swap(arr[i], arr[j]);
            i++;
        }
    }
    swap(arr[i-1], arr[start]);
    return i-1;
}

void quicksort(vector<string> &arr, int start, int end){
    if(start < end) {
        int piv_pos = partition(arr, start, end);
        quicksort(arr, start, piv_pos - 1);
        quicksort(arr, piv_pos + 1, end);
    }
}
int main(){
    int n;
    cin>>n;
    while(n--) {
        string temp;
        getline(cin, temp);
        stringstream ss(temp);
        vector<string> arr{istream_iterator<string>{ss}, istream_iterator<string>{}};
        //while(ss>>temp) arr.emplace_back(temp);
        quicksort(arr, 0, arr.size()-1);
        for (auto item: arr) {
            cout << item<< " ";
        }
        cout<<endl;
    }
}

