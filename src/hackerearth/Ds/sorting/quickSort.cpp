//
// Created by Akhilesh on 12-09-2017.
//
#include<bits/stdc++.h>
using namespace std;

int partition(vector<int> &arr, int start, int end){
    int piv = arr[start];
    int i =start+1;
    for(int  j=start+1; j<=end; j++){
        if(arr[j]<piv){
            swap(arr[i], arr[j]);
            i++;
        }
    }
    swap(arr[i-1], arr[start]);
    return i-1;
}

void quicksort(vector<int> &arr, int start, int end){
    if(start < end) {
        int piv_pos = partition(arr, start, end);
        quicksort(arr, start, piv_pos - 1);
        quicksort(arr, piv_pos + 1, end);
    }
}
int main(){
    int n;
    cin>>n;
    vector<int> arr(n);
    for (int j = 0; j < n; ++j) {
        cin>>arr[j];
    }
    quicksort(arr, 0, n-1);
    for (int i = 0; i < n; ++i) {
        cout<< arr[i] <<" ";
    }
}
