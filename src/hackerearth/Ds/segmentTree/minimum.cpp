//
// Created by akhilesh on 15/11/17.
//

//buggy
#include <bits/stdc++.h>
using namespace std;

void buildTree(int arr[], int tree[], int node, int start, int end){
    if(start == end){
        tree[node] = arr[start];
    }else{
        int mid = (start+end)/2;
        buildTree(arr, tree, 2*node+1, start, mid);
        buildTree(arr, tree, 2*node+2, mid+1, end);
        tree[node] = min(tree[2*node+1], tree[2*node+2]);
    }

}

void update(int arr[], int tree[], int node, int idx, int val, int start, int end){
    if(start == end){
        arr[idx] = val;
        tree[node] = val;
    }else{
        int mid = (start+end)/2;
        if(idx>=start &&  idx<=mid){
            update(arr, tree, 2*node+1, idx, val, start, mid);
        }else{
            update(arr, tree, 2*node+2, idx, val, mid+1, end);
        }
        tree[node] = min(tree[2*node+1], tree[2*node+2]);
    }
}

int query(int tree[], int node, int start, int end, int l, int r){
    if(r < start || l > end) return INT_MAX;
    if(l<= start && end <= r) return tree[node];
    int mid = (start + end)/2;
    return min(query(tree, 2*node+1, start, mid,l,r),query(tree,2*node+2, mid+1, end, l, r) );

}

int main(){
    int n, q;
    cin >> n >> q;
    int arr[n], tree[2*n -1];
    for (int j = 0; j <n ; ++j) {
        cin>>arr[j];
    }
    buildTree(arr, tree,  0, 0, n-1);
    while (q--){
        char choice;
        cin >> choice;
        int c1, c2;
        cin >> c1 >> c2;
        if(choice == 'q'){
            cout << query(tree, 0, 0, n-1, c1-1, c2-1) << endl;
        }else{
            update(arr,tree,0, c1-1, c2, 0, n-1);
        }
    }
    return 0;
}
