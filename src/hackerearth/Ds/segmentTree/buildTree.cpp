//
// Created by akhilesh on 14/11/17.
//
#include <bits/stdc++.h>
using namespace std;

void buildtree(int arr[], int tree[], int node, int start, int end){
    if(start == end){
        tree[node] = arr[start];
    }else{
        int mid = (start + end)/2;
        buildtree(arr, tree, 2*node +1, start, mid);
        buildtree(arr, tree, 2*node +2, mid+1, end);
        tree[node]= tree[2*node+1] + tree[2*node + 2]; //? tree[2*node +1] : tree [2*node + 2];
    }
}

void update(int tree[], int cnode, int val, int idx, int start, int end){
    if( start == end){
        tree[ cnode ] = val;
    } else {
        int mid = (start + end) / 2;
        if (start <= idx && idx <= mid) {
            update(tree, 2*cnode +1, val, idx, start, mid );
        } else if (idx > mid && idx <= end) {
            update(tree, 2*cnode+2, val, idx, mid+1, end);
        }
        tree[cnode] = tree[2*cnode+1] + tree[2*cnode +2];
    }
}

int query(int tree[], int node, int start, int end, int l, int r){
    if(r< start or end < l) return 0;

    if(l <= start && r >= end) return tree[node];

    int mid = (start+end)/2;
    return query(tree,2*node+1, start, mid, l, r) + query(tree, 2*node+2, mid+1, end, l, r);
}

int main(){
    int n;
    cin>>n;
    int arr[n];
    for(int i=0;i<n;i++){
        cin>>arr[i];
    }
    int tree[2*n - 1];
    buildtree(arr, tree, 0, 0, n-1);
    for(int i: tree){
        cout << i << " ";
    }
    cout<<endl;
    //update(tree, 0, 10, n-1, 0, n-1);
    for(int i: tree){
        cout << i << " ";
    }

    while(true){
        int l, r;
        cin >> l >> r;
        cout << query(tree, 0, 0, n-1, l, r) <<endl;
    }
}
