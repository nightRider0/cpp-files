//
// Created by Akhilesh on 31-08-2017.
//

#include<bits/stdc++.h>
using namespace std;

int  root(const int arr[], int size , int number){
    while(arr[number] != number ){
        number = arr[number];
    }
    return number;
}

bool unionx(int arr[], int size[], int n, int a, int b){
   try {
       a = root(arr, n, a);
       b = root(arr, n, b);
       if (size[a] < size[b]) {
           arr[a] = b;
           size[a]++;
       } else {
           arr[b] = a;
           size[b]++;
       }
       return true;
   }catch (exception e){
       cout<<e.what()<<endl;
       return false;
   }

}

bool related(int arr[], int n, int a, int b){
    a = root(arr, n, a);
    b = root(arr, n, b);
    return a==b;
}

template<class T >
bool print(T arr[], int n) {
    try {
        for (int i = 0; i < n; i++) {
            cout << arr[i] << " ";
        }
        cout << endl;
        return true;
    }catch(exception e){
        cout<< e.what() <<endl;
        return false;
    }
}

int main() {

    int n;
    cin >> n;
    int arr[n], size[n];
    for (int i = 0; i < n; i++) {
        arr[i] = i;
    }
    for (int j = 0; j < n; ++j) {
        size[j] = 1;
    }
    
    int i = 10;
    
    while(i--){
        int op, a, b;
        cin>> op >> a >> b;
        if(op==0){
            cout<<unionx(arr, size, n, a, b);
        }else if( op == 1){
            cout<<related(arr, n, a, b);
        }else{
            print(arr,n);
        }
    }

}