//
// Created by Akhilesh on 12-09-2017.
//
#include <bits/stdc++.h>
using namespace std;

void merge(long arr[], long result[], int start, int mid, int end){
    mid++;
    for(int i = start; i<mid ;i++){
        for(int j =mid; j<=end; j++){
            if(result[i] > arr[j]){
                result[i]++;
            }
        }
    }
}

void split(long arr[], long result[],int start,  int end){
    if(start >= end){
        return ;
    }else{
        int mid = (start+end)/2;
        split(arr, result, start,mid);
        split(arr, result, mid+1, end);
        merge(arr, result, start, mid, end);
    }
}

int main()
{
    int n;
    scanf("%d", &n);
    long arr[n];
    long result[n];
    for( int i=0; i<n; i++){
        scanf("%lu",&arr[i]);
        result[i] = arr[i];
    }
    split(arr, result, 0, n-1);
    for(int i =0; i<n; i++){
        printf("%lu ",result[i]);
    }
    printf("\n");
}