#include<bits/stdc++.h>
using namespace std;
int arr[10000][10000];
int sol(int mi,int n){
    queue<int>q;
    int visit [n+1];
    int ctr=0;
    q.push(mi);
    q.push(-10);
    while(!q.empty()){
        int temp=q.front();
        q.pop();
        if(visit[temp]&&temp!=-10)continue;
        if(temp==-10){
                ctr++;
                q.push(-10);
                continue;
        }
        visit[temp]=true;

        if(temp==n)return ctr;

        for(int i=1;i<n+1;i++){
            if(arr[temp][i]==1)q.push(i);
        }
    }
}
int main(){
    int t;
    cin>>t;
    while(t--){
        int n,m,mi=INT_MAX;
        for(int i=0;i<m;i++){
            int x,y;
            cin>>x>>y;
            mi=min(mi,min(x,y));
            arr[x][y]=1;
            arr[y][x]=1;
        }
        cout<<sol(mi,n)<<endl;
    }
}
