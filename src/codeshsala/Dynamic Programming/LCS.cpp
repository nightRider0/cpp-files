#include <bits/stdc++.h>
using namespace std;
int Sol(string X,string Y){
    int result=0;
    int dp[X.length()+1][Y.length()+1];
    for(int i=0;i<=X.length();i++){
        for(int j=0;j<=Y.length();j++){
            if(i==0||j==0){
                dp[i][j]=0;
            }
            else if(X[i-1]==Y[j-1]){
                dp[i][j]=dp[i-1][j-1]+1;
                result=max(result,dp[i][j]);
            }
            else dp[i][j]=0;

        }
    }
    return result;
}
int main(){
    string X;
    string Y;
    cin>>X>>Y;
    cout<<Sol(X,Y)<<endl;
}