#include<bits/stdc++.h>
using namespace std;
map<int,int>dp;
int sol(int *arr,int n,int s){
    if(s<0)return INT_MAX-20;

    if(s==0)return 0;

    int countr=INT_MAX-20;

    if(dp[s]!=0)return dp[s];

    for(int i=0;i<n;i++){
        //cout<<"rupee"<<arr[i]<<"  amount: "<<s<<endl;
        countr=min(countr,sol(arr,n,s-arr[i])+1);
    }
    dp[s]=countr;
    return countr;
}
int main(){
    int n,s;
    cin>>n>>s;
    int arr[n];
    for(int i=0;i<n;i++){
        cin>>arr[i];
        dp[arr[i]]=1;
    }
    sort(arr,arr+n);
    cout<<sol(arr,n,s)<<endl;
}

