#include<iostream>
#include<map>
using namespace std;
int larseq(int*arr,int ptr,int n,map<int,int>dp){
    int value=0;
    //if(ptr==n)return 0;
   // cout<<"point 1";
    if(dp[arr[ptr]]!=0){
        return dp[arr[ptr]];
    }
    for(int i=ptr;i<n;i++){
            if(arr[ptr]<arr[i])
            value=max(value,larseq(arr,i,n,dp)+1);
    }
    dp[arr[ptr]]=value;
    return value;
}

int main(){
    int n;
    cin>>n;
    int arr[n];
    for(int i=0;i<n;i++){
        cin>>arr[i];
    }
    map<int,int>dp;
    for(int i=0;i<n;i++){
        dp[arr[i]]=0;
    }
    larseq(arr,0,n,dp);
    int maxi=-9999;
    for(int i=0;i<n;i++){
        maxi=max(maxi,dp[arr[i]]);
    }
    cout<<maxi;
}
