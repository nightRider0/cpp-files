#include<bits/stdc++.h>
using namespace std;
int sol(int *dp,int* arr,int n){
    int maxi=0;
    dp[0]=1;
    for(int i=1;i<n;i++){
        for(int j=0;j<i;j++){
            if(arr[i]>arr[j]){
                dp[i]=max(dp[i],dp[j]+1);
                maxi=max(maxi,dp[i]);
            }
        }
        dp[i]=max(dp[i],1);
    }
    return maxi;
}
int main(){
    int n;
    cin>>n;
    int arr[n];
    for(int i=0;i<n;i++){
        cin>>arr[i];
    }
    int dp[n]={0};
    cout<<sol(dp,arr,n);
}
