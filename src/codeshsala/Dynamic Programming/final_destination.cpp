#include <bits/stdc++.h>
using namespace std;
int sol(int a,int b,int mul,int div,int add,int sub){
        int ctr=0;
        int arr[1000001]={0};
        arr[a]=1;
        queue<int>q;
        q.push(a);
        q.push(-10);
        while(!q.empty()){
            int temp=q.front();
           // cout<<"temp:"<<temp<<endl;
            if(temp==b){
                return ctr;
            }
            //arr[temp]=1;
            q.pop();
            if(temp==-10){
                    ctr++;
                    if(q.empty())break;
                    q.push(-10);
                    continue;
            }
            int nmul=temp*mul,ndiv=temp/div,nadd=temp+add,nsub=temp-sub;
            if(nmul>=2&&nmul<=1000000)if(!arr[nmul]){arr[nmul]=1;q.push(nmul);}
            if(ndiv>=2&&ndiv<=1000000)if(!arr[ndiv]){arr[ndiv]=1;q.push(ndiv);}
            if(nadd>=2&&nadd<=1000000)if(!arr[nadd]){arr[nadd]=1;q.push(nadd);}
            if(nsub>=2&&nsub<=1000000)if(!arr[nsub]){arr[nsub]=1;q.push(nsub);}
        }
        return -1;
}
int main(){
    int t,a,b,mul,div,add,sub;
    cin>>t;
    while(t--){
        cin>>a>>b>>mul>>div>>add>>sub;
        cout<<sol(a,b,mul,div,add,sub);
    }
}
