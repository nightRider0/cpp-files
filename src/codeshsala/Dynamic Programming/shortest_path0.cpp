#include<bits/stdc++.h>
using namespace std;
int arr[10][10]={0};
int visit[10][10]={0};
int sol(int n,int x,int y){
    if(y+1==n)return 1;
    visit[x][y]=1;
    int dis=INT_MAX-20;
    for(int i=y;i<n;i++){
        if(visit[x][i]==1)continue;
        if(arr[x][i]==1)dis=min(dis,sol(n,i,x)+1);

    }
    visit[x][y]=0;
    return dis;
}
int main(){
    int t,n,m;
    cin>>t>>n>>m;
    for(int i=0;i<m;i++){
        int x,y;
        cin>>x>>y;
        arr[x][y]=1;
        arr[y][x]=1;
    }
    cout<<sol(n,0,0);
}

