#include<bits/stdc++.h>
using namespace std;
long mincoin(long table[],long amount,long coins[],long arrsize){
    if(amount==0){
        return 0;
    }
    if(amount<0){
        return 999999;
    }//cout<<endl<<"point1 "<<"amount: "<<amount<<" table: "<<table[amount]<<endl;
    if(table[amount]!=(999999)){
        return table[amount];
    }

    long value=999999;

    for(long i=0;i<arrsize;i++){
            //cout<<endl<<"point1 "<<"amount: "<<amount<<" table: "<<table[amount]<<" value: "<<value<<" coin[i]: "<< coins[i]<<endl;
            value=min(value,mincoin(table,amount-coins[i],coins,arrsize)+1);
            //cout<<endl<<"point2 "<<"amount: "<<amount<<" table: "<<table[amount]<<" value: "<<value<<" coin[i]: "<<coins[i]<<endl;

    }
    table[amount]=value;
    return value;
}
int main(){
    long n;
    cin>>n;
    long amount;
    cin>>amount;
    long table[amount+1];
    for( long i=0;i<=amount;i++ ){table[i]=999999;}
    //cout<< sizeof(table);
    long arr[n];
    for(long i=0;i<n;i++){
        cin>>arr[i];
    }
    long temp=mincoin(table,amount,arr,n);
    temp==999999?cout<<-1<<endl:cout<<temp<<endl;
}
