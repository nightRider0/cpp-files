#include<bits/stdc++.h>
using namespace std;
int sol(vector<vector<int > >adl,int mi,int n){
    queue<int>q;
    map<int , bool> visit;
    int ctr=0;
    q.push(mi);
    q.push(-10);
    while(!q.empty()){
        int temp=q.front();
        q.pop();
        if(visit[temp]&&temp!=-10)continue;
        if(temp==-10){
                ctr++;
                q.push(-10);
                continue;
        }
        visit[temp]=true;

        if(temp==n)return ctr;

        for(int i=0;i<adl[temp].size();i++){
            q.push(adl[temp][i]);
        }
    }
}
int main(){
    int t;
    cin>>t;
    while(t--){
        int n,m,mi=INT_MAX;
        vector<vector<int> >adl(n+1);
        for(int i=0;i<m;i++){
            int x,y;
            cin>>x>>y;
            mi=min(mi,min(x,y));
            adl[x].push_back(y);
            adl[y].push_back(x);
        }
        cout<<sol(adl,mi,n)<<endl;
    }
}
