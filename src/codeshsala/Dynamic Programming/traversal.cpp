#include<bits/stdc++.h>
using namespace std;
int arr[100][100];
bool visited[100][100];
queue<int>q;
void print_dfs(int n,int i,int j)
{
    q.push(0);
    visited[i][j]=true;
    cout<<i<<" ";
    while(!q.empty()){
        int l=q.front();q.pop();
        for(int k=0;k<n;k++){
            if(visited[l][k]==true)continue;

            if(arr[l][k]==1){
                cout<<k<<"  ";
                q.push(k);
                visited[l][k]=true;
                visited[k][l]=true;
            }
        }
    }

}
int main(){
    int n,m;
    cin>>n>>m;
    for(int i=0;i<m;i++){
        int x,y;
        cin>>x>>y;
        arr[x][y]=1;
        arr[y][x]=1;
    }
    print_dfs(n,0,0);
}
