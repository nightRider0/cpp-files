#include<iostream>
#include<map>
using namespace std;
struct node
{
    int data;
    node * next;
};
node * llist()
{
    int n;
    cout<<"number of items you want to enter\n";
    cin>>n;
    node* start=0,*last=0,*temp=0;
    while(n--)
    {
        temp=new node;
        cin>>temp->data;
        temp->next=0;
        if(start==0)
        {
            start=temp;
            last=temp;
        }
        else
        {
            last->next=temp;
            last=last->next;
        }
    }
    return start;
}
int main()
{
    node *list1,*list2;
    list1=llist();
    list2=llist();
    bool flag=true;
    map<int, int > newmap;
    while(list1)
    {
        newmap[list1->data]=1;
        list1=list1->next;
    }
    while(list2)
    {
        if(newmap[list2->data])
        {
            cout<<list2->data<<" ";
            flag=false;
        }
        list2=list2->next;
    }
    if(flag)
    {
        cout<<-1;
    }
    cout<<endl;
}
