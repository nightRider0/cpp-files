#include<iostream>
#include<math.h>
using namespace std;
int quadratic(int k,int A,int B,int C)
{
    return (A*(k*k))+(B*k)+C;
}
int xfind(int lo, int hi,int value,int A,int B,int C)
{
    int prev,sol,mid;
        while(true)
        {
            mid=(hi+lo)/2;
            if(quadratic(mid,A,B,C)==value)
            {
                sol=mid+1;
                return sol;
            }
            else if(quadratic(mid,A,B,C)>value)
            {
                sol=mid;
                hi=mid-1;
            }
            else if(quadratic(mid,A,B,C)<value)
            {
                lo=mid+1;
            }
            if(lo>hi)
                return sol;

        }


}
int main()
{
    int t,A,B,C;
    cout<<"enter count of test case\n";
    cin>>t;
    cout<<"enter A,B,C:enter positive value\n";
    cin>>A>>B>>C;
    while(t--)
    {
        int value,sol;
        cout<<"enter value";
        cin>>value;
        int k=sqrt(value);
        //int xfind(int lo, int hi,int value,int A,int B, int C)
        sol=xfind(0,k+1,value,A,B,C);
        cout<<sol<<endl;

    }
}
