#include <iostream>
#include <cmath>
#include <climits>
using namespace std;
long quadratic(long a,long b,long c,long x)
{
	a=a*x;
	a=a*x;
	b=b*x;
	return a+b+c;
}

long search(long a,long b, long c,long k)
{
	long lo=sqrt(k/a)-1,hi=lo+b+c,mid,previous=LONG_MAX,x,test;
	if(lo<0)
	lo=0;
	while(lo<=hi)
	{
		mid=(lo+hi)/2;
		//cout<<" low high mid "<<lo<<" "<<hi<<" "<<mid<<endl;
		test=quadratic(a,b,c,mid);
		if(test==k)
		{
			return mid;
		}
		else if(test>k)
		{
			if(test<previous)
			{
				previous=test;
				x=mid;
			}
			hi=mid-1;
		}
		else
		{
			lo=mid+1;
		}
	}
	return x;
}
int main()
{
    long n;
    cin>>n;
    long a,b,c,k;
    for(long i=0;i<n;i++)
    {
    	cin>>a>>b>>c>>k;
    	cout<<search(a,b,c,k)<<endl;


    }
    return 0;
}
