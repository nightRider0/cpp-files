#include <iostream>
#include<queue>
using namespace std;
struct node
{
	long  data;
	node * left;
	node * right;
};
node* create(long value)
{
	node *temp=new node;
	temp->right=0;
	temp->left=0;
	temp->data=value;
	return temp;
}
long sol(int value)
{
	queue<node*> q;
	node *root,*temp;
	root=create(1);
	q.push(root);
	while(true)
	{
		temp=q.front();
		q.pop();//cout<<"marker"<<temp->data<<endl;
		if(temp->data%value==0) return temp->data;
		temp->right=create((temp->data*10)+1);
		temp->left=create(temp->data*10);
		q.push(temp->left);
		q.push(temp->right);
	}
}
int main()
{
	int n;
	cin>>n;
	while(n--)
	{
		int value;
		cin>>value;
		cout<<sol(value)<<endl;
	}
}
