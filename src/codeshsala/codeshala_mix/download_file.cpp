#include<bits/stdc++.h>
using namespace std;
int main(){
    int t;
    cin>>t;
    while(t--){
        int n,k,Ti,Di;
        cin>>n>>k;
        int sum=0;
        int dis=0;
        for(int i=0;i<n;i++){
            cin>>Ti>>Di;
            sum+=Ti*Di;
            if(k>0){
                if(Ti>k){
                    dis+=(Ti-k)*Di;
                    }
                else if(Ti<k){
                    dis+=(k-Ti)*Di;
                }
                else if(Ti==k){
                    dis+=k*Di;
                }
                k-=Ti;
            }
        }
        cout<<sum-dis<<endl;

    }
}
