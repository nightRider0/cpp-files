#include<bits/stdc++.h>
using namespace std;
vector<int> sol(int n){
    vector<int> vec,temp;
    if(n==0){
        vec.push_back(0);
        return vec;
    }
    if(n==1){
        vec.push_back(1);
        return vec;
    }
    temp=sol(n/2);
    vec=temp;
    vec.push_back(n%2);
    vector<int>::iterator it;
    for(it=temp.begin();it!=temp.end();++it){
        vec.push_back(*it);
    }
    return vec;
}
int main(){
    int n,l,r;
    cin>>n>>l>>r;
    int sum_of_element;
    vector<int> vec=sol(n);
    sum_of_element=accumulate(vec.begin()+(l-1),vec.begin()+(r),0);
    //cout<<*vec.begin()+(l-1)<<"  "<<*vec.begin()+(r-1)<<endl;
    cout<<sum_of_element<<endl;
}

