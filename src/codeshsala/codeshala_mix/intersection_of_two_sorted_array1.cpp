#include<bits/stdc++.h>
using namespace std;
bool __search(long *arr,int x,int hi){
    int lo=0,mid;
    while(lo<=hi){
        mid=(lo+hi)/2;
        if(arr[mid]==x)return true;
        if(arr[mid]<x)lo=mid+1;
        else hi=mid-1;
    }
    return false;
}
int main(){
    int n;
    cin>>n;
    long arr1[n];
    vector <long> vec;
    for(int i=0;i<n;i++){
        cin>>arr1[i];
    }
    int m;
    cin>>m;
    long arr2[m];
    for(int i=0;i<m;i++){
        cin>>arr2[i];
    }
    for(int i=0;i<n;i++){
        if(__search(arr2,arr1[i],n-1))vec.push_back(arr1[i]);
    }
    if(vec.empty())cout<<-1<<endl;
    else{
        sort(vec.begin(),vec.end());
        vec.erase(unique(vec.begin(),vec.end()),vec.end());
        vector<long>::iterator it;
        for(it=vec.begin();it!=vec.end();it++){
                cout<<*it<<" ";
        }
        cout<<endl;
    }
}
