#include<bits/stdc++.h>
using namespace std;
long long sol(long long query,long long*arr,long long int n ){
    int lo=0,hi=n-1,mid;
    while(lo<=hi){
        mid=(lo+hi)/2;
        if(arr[mid]==query){
            return arr[mid];
        }
        if(arr[mid]<query&&mid+1<n&&arr[mid+1]>query){
            return arr[mid+1];
        }
        if(arr[mid]<query&&mid+1<n&&arr[mid+1]<query){
            lo=mid+1;
            //continue;
        }
        if(arr[mid]>query&&mid-1>=0&&arr[mid-1]<query){
            return arr[mid];
        }
        if(arr[mid]>query&&mid-1>=0&&arr[mid-1]>query){
            hi=mid-1;
        }
    }
    return -1;
}
int main(){
    long long n;
    cin>>n;
    long long arr[n];
    for(long long i=0;i<n;i++){
        cin>>arr[i];
    }
    sort(arr,arr+n);
    long long queries;
    cin>>queries;
    while(queries--){
        long long query;
        cout<<sol(query,arr,n);
    }
}