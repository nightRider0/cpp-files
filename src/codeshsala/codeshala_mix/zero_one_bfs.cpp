#include<bits/stdc++.h>
using namespace std;
string sol(long long n){
    queue<pair<long long,string> > q;
    if(1%n==0)return "1";
    q.push(make_pair(1,"1"));
    while(!q.empty()){
        pair<long long,string>temp;
        temp=q.front();
        q.pop();
        long long rem=temp.first;
        string st=temp.second;
        //cout<<st<<endl;
        //multiply by 10
        rem=(rem%n*10%n)%n;
        if(rem==0)return st+"0";
        q.push(make_pair(rem,st+"0"));
        //multiply by 11
        rem=(rem%n+1%n)%n;
        if(rem==0)return st+"1";
        q.push(make_pair(rem,st+"1"));
    }
    return "-10";
}
int main(){
    long long n,t;
    cin>>t;
    while(t--){
            cin>>n;
    //cout<<"n is: "<<t<<" with sol: "<<sol(t)<<endl;
        cout<<sol(n)<<endl;
    }
}
