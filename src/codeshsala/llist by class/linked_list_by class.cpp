#include<iostream>
using namespace std;
class Node
{
    friend class Linkedlist;
private:
    int _data;
    Node* _next;
protected:
    Node(void):_next(0){}
    Node(Node* next):_data(0),_next(next){}
    Node(int data,Node* next):_data(data),_next(next){}
    Node(int data):_data(data),_next(0){}
    void Next_update(Node* next)
    {
        _next=next;
    }
    int getvalue(void){return _data;}
    Node* getnext(void){return _next;}
};
class Linkedlist
{
   // friend class Node;
private:
    Node * head,*tail;
public:
    Linkedlist(void)
    {
        head=tail=0;
    }
    Linkedlist(int val)
    {
        head=new Node(val);
        tail=head;
    }
    void Push_back(int val)
    {
        if(!head&&!tail)
        {
            tail=new Node(val);
            head=tail;
        }
        else
        {
            Node *temp=new Node(val);
            tail->Next_update(temp);
            tail=tail->getnext();
        }
    }
    void Push_front(int val)
    {
        if(head==0&&tail==0)
        {
            head=new Node(val);
            tail=head;
        }
        Node *temp=new Node(val,head);
        head=temp;
    }
    void Print_list(void)
    {
        cout<<endl<<"following is our list"<<endl;
        Node *temp=head;
        while(temp)
        {
            cout<<temp->getvalue()<<" ";
            temp=temp->getnext();
        }
    }
};
int main()
{
    Linkedlist l1;
    l1.Push_back(5);
    l1.Push_back(10);
    l1.Push_front(6);
    l1.Push_front(7);
    //cout<<l1.tail;
    l1.Print_list();
}
