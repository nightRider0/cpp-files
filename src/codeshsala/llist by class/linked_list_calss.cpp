#include<iostream>
using namespace std;
class node
{
public :
    int data;
    node *next;
    node(int value)
    {
        data=value;
        next=0;
    }
};
class llist
{
public:
    node* head,*tail;
    llist()
    {
        head=0;
        tail=0;
    }
    void insert_front(int d)
    {
        if(head==0)
        {
            head=tail=new node(d);
        }
        else
        {
            node* t=new node(d);
            t->next=head;
            head=t;
        }
    }
    void insert_back(int d)
    {
        if(tail==0)
        {
            head=tail=new node(d);
        }
        else
        {
            node* t=new node(d);
            tail->next=t;
            tail=t;
        }
    }
    void print()
    {
        node* t =head;
        while (t)
        {
            cout<<t->data<<" ";
            t=t->next;
        }
    }
};
int main()
{
    llist list1;
    list1.insert_back(1);
    list1.insert_back(2);
    list1.insert_back(3);
    list1.insert_back(4);
    list1.insert_back(5);
    list1.insert_back(6);
    list1.insert_back(7);
    list1.print();
}
