#include<bits/stdc++.h>
using namespace std;
void maxheapyfy(int arr[],int i,int n){//down to top
    if(i==0)return;
    int parent=(i-1)/2;
    if(arr[i]>arr[parent]){
        swap(arr[i],arr[parent]);
        maxheapyfy(arr,parent,n);
    }
}
void minheapyfy(int arr[],int i,int n){//down to top
    if(i==0)return;
    int parent=(i-1)/2;
    if(arr[i]<arr[parent]){
        swap(arr[i],arr[parent]);
        minheapyfy(arr,parent,n);
    }
}

int main(){
    int n;
    cin>>n;
    int arr[n];
    for(int i=0;i<n;i++){
        cin>>arr[i];
        minheapyfy(arr,i,n);
    }
    for(int i=0;i<n;i++)
        cout<<arr[i]<<" ";
}
