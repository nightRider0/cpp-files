#include<bits/stdc++.h>
using namespace std;
int fact(int n){
    int sum=n;
    if(n==0||n==1)return 1;
    return n*fact(n-1);
}
int moduloexpo(int a,int b){
        if(b<0)return -1;
        if(b==0)return 1;
        if(b==1)return a;
        if(b%2)
        return moduloexpo(a,b%2)*moduloexpo(a,b-1);
        else return moduloexpo(a,b/2)*moduloexpo(a,b/2);
}
int main(){
    int n,r,p;
    cin>>n>>r>>p;
    cout<<((fact(n)%p)*(moduloexpo(fact(r),p-2)%p)*(moduloexpo(fact(n-r),p-2)%p)%p)<<endl;
    }

