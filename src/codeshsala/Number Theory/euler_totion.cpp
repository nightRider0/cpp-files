//number of co-prime number before A given number
#include<bits/stdc++.h>
using namespace std;
int gcd(int a,int b){
    if(b==0)return a;
    return gcd(b,a%b);
}
int main(){
    int n;
    cin>>n;
    int cnt=0;
    for(int i=1;i<n;i++){
        if(gcd(n,i)==1){cnt++;cout<<"("<<n<<","<<i<<")"<<endl;}
    }
    cout<<cnt<<endl;
}
