//
// Created by Akhilesh on 22-Apr-17.
//
#include <bits/stdc++.h>

using namespace std;

int expo(int a,int b){
    if(b==0)return 1;
    if(b==1) return a;
    int c=1,temp=0;
    int result=1;
    while(c){
        temp=(b&c);
        cout<<"temp: "<<temp<<endl;
        if(temp==0)continue;
        if(temp%2==0){
            result*=(a*(expo(a,temp-1)%1000000007));
        }
        else {
            result *= expo(a, temp) % 1000000007;
        }
        c=c<<1;
    }
    return result;

}

int main() {
    int a,b;
    cin>>a>>b;
    cout<<expo(a,b);
}