//array must be sorted
#include<iostream>
using namespace std;
int main()
{
    int xsize,value;
    cout<<"enter length of array \t";
    cin>>xsize;
    cout<<"enter the value \t";
    cin>>value;
    int arr[xsize];
    for(int i=0;i<xsize;i++)
    {
        cin>>arr[i];
    }
    bool lessr=false,greatr=false;
    int starter=0,ender=xsize-1;
    while(true)
    {
        if(arr[starter]+arr[ender]<value)
        {
            starter++;
            lessr=true;
        }
        else if(arr[starter]+arr[ender]>value)
        {
            ender--;
            greatr=true;
        }
        else if(lessr==true&&greatr==true||starter>=ender)
        {
            cout<<"No";
            break;
        }
        else if(arr[starter]+arr[ender]==value)
        {
            cout<<"Yes"<<"  "<<arr[starter]<<","<<arr[ender];
            break;
        }
    }
    return 0;
}
