#include<bits/stdc++.h>
#include<string>
using namespace std;
bool select(string a,string  b,int ai=0,int bi=0){
    char ha=(char)a[ai],hb=(char)b[bi];
    for(;true;ai++,bi++){
        if(ai==a.length()){
                if(b[bi]>ha)return false;
                if(b[bi]==ha){
                    return select(a,b,0,bi);
                }
                return true;
        }
        if(bi==b.length()){
                if(a[ai]>hb)return true;
                if(a[ai]==hb){
                    return select(a,b,ai,0);
                }
                return false;
        }
        if(a[ai]<b[bi])return false;
        if(a[ai]>b[bi])return true;
        //pa=(char)a[i];
        //pb=(char)b[i];
    }
}

void swap(string &a,string &b)
{
    string t = a;
    a = b;
    b = t;
}

int partition (string arr[], int low, int high)
{
    string pivot = arr[high];    // pivot
    int i = (low - 1);  // Index of smaller element

    for (int j = low; j <= high- 1; j++)
    {
        // If current element is smaller than or
        // equal to pivot
        if (select(arr[j],pivot))
        {
            i++;    // increment index of smaller element
            swap(arr[i], arr[j]);
        }
    }
    swap( arr[i + 1], arr[high]);
    return (i + 1);
}

void quickSort(string arr[], int low, int high)
{
    if (low < high)
    {
        /* pi is partitioning index, arr[p] is now
           at right place */
        int pi = partition(arr, low, high);

        // Separately sort elements before
        // partition and after partition
        quickSort(arr, low, pi - 1);
        quickSort(arr, pi + 1, high);
    }
}
int main(){
    int n;
    cin>>n;
    string arr[n];
    for(int i=0;i<n;i++)
        cin>>arr[i];
    quickSort(arr,0,n-1);
    for(int i=0;i<n;i++){
        cout<<arr[i];
    }
    cout<<endl<<" ";
}
