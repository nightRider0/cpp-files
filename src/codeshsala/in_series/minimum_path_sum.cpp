#include<bits/stdc++.h>
using namespace std ;
/*int sol(vector< vector< int> > mtx,vector< vector< int> > dp,int i,int j,int n ,int m){
        if(i==n&&j==m)return mtx[n][m];
        if(dp[i][j]!=INT_MAX)return dp[i][j];
        if(i+1<=n&&j+1<=m){
            dp[i][j]=min((sol(mtx,dp,i+1,j,n,m)+mtx[i][j]),(sol(mtx,dp,i,j+1,n,m)+mtx[i][j]));
            return dp[i][j];
        }
        if(i+1<=n){
                 dp[i][j]=(sol(mtx,dp,i+1,j,n,m)+mtx[i][j]);
                return dp[i][j];
        }
        if(j+1<=m){
            dp[i][j]= (sol(mtx,dp,i,j+1,n,m)+mtx[i][j]);
            return dp[i][j];
        }
    }*/
int sol(vector< vector< int> > mtx,int i,int j,int n ,int m)
{
    for(int i=1;i<=m;i++){
        mtx[0][i]+=mtx[0][i-1];
    }
    for(int i=1;i<=n;i++){
        mtx[i][0]+=mtx[i-1][0];
    }
    for(int i=1;i<=n;i++){
        for(int j=1;j<=m;j++){
            mtx[i][j]+=min(mtx[i-1][j],mtx[i][j-1]);
        }
    }
    return mtx[n][m];
}
int main(){
    int n,m;
    cin>>n>>m;
    vector < vector< int > > dp(n);
    vector< vector< int > > mtx(n);
    for(int i=0;i<n;i++){
        for(int j=0;j<m;j++){
            int temp=0;
            cin>>temp;
            mtx[i].push_back(temp);
            dp[i].push_back(INT_MAX);
        }
    }
    cout<<sol(mtx,0,0,n-1,m-1);

}
