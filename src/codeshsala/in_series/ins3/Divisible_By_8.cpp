//
// Created by Akhilesh on 15-Apr-17.
//
#include <bits/stdc++.h>

using namespace std;

bool checker(string s,int ptr1,int ptr2){
    int d1=s[ptr1]-'0';
    int d2=s[ptr2]-'0';
    int d=d1*10+d2;//cout<<d<<endl;
    if(d%8==0){
        return true;
    }
    return  false;
}
bool checker_single(string s,int ptr2){
    int d=s[ptr2]-'0';
    return d==8?true:false;
}
int main() {
    string s="";
    cin>>s;
    if(s.length()%2!=0){
        s="0"+s;
    }
    int ptr1,ptr2;
    ptr2=s.length()-1;
    ptr1=ptr2-1;
    int counter=0;
    while(ptr1>=0){
        if(checker(s,ptr1,ptr2)){
           //cout<<counter<<" hit"<<endl;
            if(s[0]=='0')
            counter+=ptr1;
            else
                counter+=ptr2;
        }
        else if(checker_single(s,ptr2)){
            counter++;
        }
        ptr1--;
        ptr2--;
    }
    cout<<counter<<endl;
}
