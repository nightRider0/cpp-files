#include<bits/stdc++.h>
using namespace std;
int soln(int *arr,int low,int hi,int x){
    int mid=0;
    while(low<=hi){
        mid=(low+hi)/2;//cout<<arr[mid]<<" "<<mid<<endl;
        if((arr[mid]>=x&&mid-1>=0&&arr[mid-1]<x)||(arr[mid]>=x&&mid-1<0)){//cout<<"point"<<endl;
                return arr[mid];
        }
        if(arr[mid]<x){
            low=mid+1;continue;
        }
        if(arr[mid]>x){
            hi=mid-1;continue;
        }
    }
    return -1;
}
int main(){

    int n,x;
    cin>>n>>x;
    int arr[n];
    for(int i=0;i<n;i++)cin>>arr[i];
    cout<<soln(arr,0,n-1,x)<<endl;
}
