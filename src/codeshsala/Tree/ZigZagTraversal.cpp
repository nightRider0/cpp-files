//
// Created by Akhilesh on 14-Apr-17.
//
#include <iostream>
#include <algorithm>
#include <vector>
#include <map>
#include <queue>
#include <stack>
using namespace std;
#define ll long long int
#define f_in(st) freopen(st,"r",stdin);
#define f_out(st) freopen(st,"w",stdout);
struct node {
    int data;
    struct node * left;
    struct node * right;
} * root = NULL;

struct node * insertNode(struct node * tNode, int value) {
    if(tNode == NULL) {
        struct node * newNode = (struct node *)malloc(sizeof(struct node));
        newNode->data = value;
        newNode->left = NULL;
        newNode->right = NULL;
        return newNode;
    }
    if(tNode->data > value)
        tNode->left = insertNode(tNode->left, value);
    else
        tNode->right = insertNode(tNode->right, value);
    return tNode;
}

void buildTree(int a[], int N) {
    for(int i = 0; i < N; i++) {
        if(i) {
            insertNode(root, a[i]);
        } else {
            root = insertNode(NULL, a[i]);
        }
    }
}

void print_vector_reverse(vector<int> vec){
    for(int i=vec.size()-1;i>=0;i--){
        cout<<vec[i]<<" ";
    }
    vec.clear();
}
void print_tree(node* root){
    queue<pair<node*,int> > q;
    q.push(make_pair(root,0));
    int level=0;
    while(!q.empty()){
        if(level!=q.front().second){
            cout<<endl;
            level=q.front().second;
        }
        cout<<q.front().first->data<<" ";
        if(q.front().first->left) q.push(make_pair(q.front().first->left,q.front().second+1));
        if(q.front().first->right) q.push(make_pair(q.front().first->left,q.front().second+1));
        q.pop();
    }
}
/* Implement this method */
/*int zigZagTraversal(struct node * root) {
    queue<node*>q;
    vector<int>vec;
    int flag=true;
    q.push(root);
    q.push(0);
    while(!q.empty()) {
        node *temp;
        temp = q.front();
        q.pop();
        if(temp==0){
            if(q.empty()) return -1;

            if(flag)flag=false;
            else flag=true;

            if(!vec.empty()){
                print_vector_reverse(vec);
            }
            q.push(0);
        }
        if(!temp)cout<<temp<<"temp "; else cout<<temp->data<<" temp data ";cout<<flag<<" flag"<<endl;

        if (flag) {
            vec.push_back(temp->data);
        } else{
            cout<<temp->data;
        }
        if(temp->left)q.push(temp->left);
        if (temp->right)q.push(temp->right);

    }
}
*/
int main() {
    int N;
    cin >> N;
    int arr[N];
    for(int i = 0; i < N; i++) {
        cin >> arr[i];
    }
    buildTree(arr, N);
    print_tree(root);
    //cout<<zigZagTraversal(root)<<"return value";
    cout << endl;
}
