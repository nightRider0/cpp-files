#include<bits/stdc++.h>
using namespace std;
int main(){
    int n;
    cin>>n;
    int arr;
    int dp[n];
    for(int i=0;i<n;i++){
        cin>>arr;
        if(i==0) dp[i]=arr;
        else{
            dp[i]=dp[i-1]^arr;
        }

    }
    int q;
    cin>>q;
    while(q--){
        int l,r;
        cin>>l>>r;
        if(l!=0)
        cout<<(dp[l-1]^dp[r])<<endl;
        else cout<<dp[r]<<endl;
    }
}