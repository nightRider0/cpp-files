//
// Created by Akhilesh on 10-07-2017.
//
#include <bits/stdc++.h>
using namespace std;

bool check(vector< vector < char> > &arr ,int i, int j){
    int sol = 0 ;
    //Check right
    for(int k=j ; k< arr[i].size(); k++){
        if(arr[i][k] == 'X'){
            sol++;
            break;
        }
    }

    //check left
    for (int l = j; l >0 ; --l) {
        if(arr[i][l] == 'X'){
            sol++;
            break;
        }
    }

    //check down
    for (int m = i; m < arr.size() ; ++m) {
        if(arr[m][j]=='X') {
            sol++;
            break;
        }
    }

    //check up
    for (int n = i; n >0 ; --n) {
        if(arr[n][j] == 'X') {
            sol++;
            break;
        }
    }

    return sol == 4;
}

int cover(vector< vector < char > > &arr ,int i, int j){
    //convert left
    for (int k = j; k >0 ; --k) {
        if(arr[i][k] != 'X'){
            arr[i][k]='X';
        } else break;
    }

    //convert down
    for (int l = i ; l < arr.size(); ++l) {
        if(arr[l][j] != 'X'){
            arr[l][j]='X';
        }else break;
    }

    //convert up
    for (int m = i; m >0 ; --m) {
        if(arr[m][j]!='X'){
            arr[m][j]='X';
        } else break;
    }

    //convert left
    for (int n = j; n < arr[i].size(); ++n) {
        if(arr[i][n]!='X'){
            arr[i][n]='X';
        }
        else{
            return n;
        }
    }
    return -1;
}

int main(){
    int row,column;
    cin>>row>>column;
    vector < vector <char> > arr(row);
    for(int i=0;i<row;i++){
        vector<char> temp;
        for(int j=0;j<column;j++){
            char t;
            cin>>t;
            temp.push_back(t);
        }
        arr.push_back(temp);
    }
    for (int i = 0; i < row; ++i) {
        for (int j = 0; j < column ; ++j) {
            if(arr[i][j]=='O'){
                if(check(arr,i,j)){
                    j=cover(arr,i,j);
                }
            }
        }
    }

    for (int i = 0; i < row ; i++) {
        for (int j = 0; j <column ; ++j) {
            cout<<arr[i][j];
        }
        cout<<endl;
    }
    return 0;
}
