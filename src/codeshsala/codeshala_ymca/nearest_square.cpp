//
// Created by Akhilesh on 27-Mar-17.
//

#include <bits/stdc++.h>
using namespace std;
int sol(int n){
    for(int i=1;true;i++){
        if((i*i)==n)return 0;
        if(i*i>n)return ((i*i-n)-(n-((i-1)*(i-1))))>0?(((i-1)*(i-1))-n):(i*i-n);
    }
}
int main(){
    for(int n=1;n<100;n++) {
        /*int n;
        cin >> n;*/
        int j = sol(n);
        if (j == 0)cout << "Yes\n";
        else {
            cout << n <<"  " <<  j << endl;
        }
    }
}
