//
// Created by Akhilesh on 08-06-2017.
//
#include <bits/stdc++.h>
using namespace std;
class xNode{
private:
    char _data;
    bool _end;
    xNode *_Left;
    xNode *_Down;
public:
    explicit xNode(char data='0'){
        xNode::_data=data;
        xNode::_end=false;
        xNode::_Left=0;
        xNode::_Down=0;
    }

    char get_data() const {
        return _data;
    }

    void set_data(char _data) {
        xNode::_data = _data;
    }

    bool is_end() const {
        return _end;
    }

    void set_end(bool _end) {
        xNode::_end = _end;
    }

    xNode *get_Left() const {
        return _Left;
    }

    void set_Left(xNode *_Left) {
        xNode::_Left = _Left;
    }

    xNode *get_Down() const {
        return _Down;
    }

    void set_Down(xNode *_Down) {
        xNode::_Down = _Down;
    }
};
class Trie{
private:
    xNode _head;
    inline xNode*insertNode(char temp){
        xNode * newnode=new xNode;
        newnode->set_data(temp);
        return newnode;
    }
public:
    explicit Trie(){};
    explicit Trie(string data){
        Trie::InsertData(data);
    }

    void InsertData(string );
    bool IsContain(string);
    void PrintTrie(void);
};
void Trie::InsertData(string data) {
    xNode * pos=&_head;
     for (int i=0;i<data.length();i++){
        if(pos->get_Down()==0){
            pos->set_Down(insertNode(data[i]));
            pos=pos->get_Down();
            if(i==data.length()-1){
                pos->set_end(true);
            }
        }
        else{
            xNode *mov=pos->get_Down();
            while(true){
                if(mov->get_data()==data[i]){
                    pos=mov;
                    if(i==data.length()-1) pos->set_end(true);
                    break;
                } else if(mov->get_Left()==0){
                    mov->set_Left(insertNode(data[i]));
                    pos=mov->get_Left();
                    if(i==data.length()-1) pos->set_end(true);
                    break;
                }
                mov=mov->get_Left();
            }
        }
    }
}
bool Trie::IsContain(string data) {
    xNode *pos=_head.get_Down();
    for (int i =0 ; i < data.length() ; i ++){
        if(pos->get_data()==data[i]){
            if((i==data.length()-1)&&pos->is_end())return true;
            pos=pos->get_Down();
        }
        else{
            pos=pos->get_Left();
            while(true){
                if(pos==0){
                    return false;
                }
                else if(pos->get_data()==data[i]){
                    if((i==data.length()-1)&&pos->is_end())
                        return true;
                    pos=pos->get_Down();
                    break;
                }
                else {
                    pos=pos->get_Left();
                }
            }
        }
    }
    return false;
}
void Trie::PrintTrie() {
    queue<xNode *>  vec;
    xNode * temp=&_head;
    if(temp->get_Down()!=0) {
        vec.push(temp->get_Down());
    }
    while(!vec.empty()){
        temp=vec.front();
        vec.pop();
        cout<<" strand started \n";
        while(temp){
            cout<<temp->get_data();
            if(temp->get_Left()){
                cout<<" branch originated ";
                vec.push(temp->get_Left());
            }
            if(temp->is_end()){
                cout<<" <end> ";
            }
            cout<<endl;
            temp=temp->get_Down();
        }
    }

}
int main(){
    Trie MobileNo;
    MobileNo.InsertData("9671056957");
    MobileNo.InsertData("9671348818");
    MobileNo.InsertData("9416889699");
    MobileNo.InsertData("9810633432");
    //cout<<(MobileNo.IsContain("9671056957")?"true":"false");
    MobileNo.PrintTrie();
}
