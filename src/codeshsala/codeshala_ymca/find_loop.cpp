//loop in undirected graph using dfs

#include<bits/stdc++.h>
using namespace std;
bool x=false;
void dfsloop(vector< vector<int > > vec,int *visit,int point,int parent){
    //cout<<point<<endl;
    if(visit[point]==1||x){x=true;return;}
    visit[point]=1;
    for(int i=0;i<vec[point].size();i++)
    {
        if(vec[point][i]==parent) continue;
        dfsloop(vec,visit,vec[point][i],point);
    }
    return;

}

int main(){
    int n,m;
    cin>>n>>m;
    vector< vector<int> >vec(n);
    //bool flag=false;
    int smallest=INT_MAX;
    int visit[n]={0};
    //int arr[n][n]={0};
   // memset(arr,0,n);
    for(int i=0;i<m;i++){
        int x,y;
        cin>>x>>y;
        vec[x-1].push_back(y-1);
        vec[y-1].push_back(x-1);
        smallest=min(smallest, min(x,y));
    }
    dfsloop(vec,visit,smallest,-1);
    x?cout<<"YES"<<endl:cout<<"NO"<<endl;


}
