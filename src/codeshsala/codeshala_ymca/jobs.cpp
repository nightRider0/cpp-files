//
// Created by Akhilesh Rao on 04-Mar-17.
//
#include <bits/stdc++.h>
using namespace std;
bool x=0;
void dfsloop(vector< vector<int > > vec,bool *visit,int point,int parent){
    if(visit[point]||x){x=true;return;}
    visit[point]=true;
    for(int i=0;i<vec[point].size();i++)
    {
        if(vec[point][i]==parent) continue;
        dfsloop(vec,visit,vec[point][i],point);
    }
    visit[point]=false;
    return;

}

void sol(vector< vector< int > > board,int i,bool arr[]){
    arr[i]=true;
    for(int j=0;j<board[i].size();j++){
        if(!arr[board[i][j]])
            sol(board,board[i][j],arr);
    }
    cout<<i+1<<" ";
    return ;
}
int main(){
    int n,m;
    cin>>n>>m;
    vector< vector< int > >board(n);
    for(int i=0;i<m;i++){
        int a,b;
        cin>>a>>b;
        board[a-1].push_back(b-1);
    }
    bool arr[n]={false};
    dfsloop(board,arr,0,-1);
    if(x)cout<<"Dead Lock"<<endl;
    else  {
        memset(arr,false,sizeof(arr));
        sol(board,0,arr);
    }
    return 0;
}
