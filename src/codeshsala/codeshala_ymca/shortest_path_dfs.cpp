//shortest path using dfs
//bug
#include<bits/stdc++.h>
using namespace std;
int dfsloop(vector< vector<int > > vec,int *visit,int point,int parent){
    //cout<<point<<endl;
    int mini=INT_MAX;
    if(visit[point]==1){return 0;}
    visit[point]=1;
    for(int i=0;i<vec[point].size();i++)
    {
        if(vec[point][i]==parent) continue;
        mini=min(mini,dfsloop(vec,visit,vec[point][i],point)+1);
    }
    visit[point]=0;
    return mini;

}
int main(){
    int n,m;
    cin>>n>>m;
    vector< vector<int> >vec(n);
    bool flag=false;
    int smallest=INT_MAX;
    int visit[n]={0};
    //int arr[n][n]={0};
   // memset(arr,0,n);
    for(int i=0;i<m;i++){
        int x,y;
        cin>>x>>y;
        vec[x].push_back(y);
        vec[y].push_back(x);
        smallest=min(smallest,x);
    }
    cout<<dfsloop(vec,visit,smallest,-1)<<endl;
    //x==1?cout<<"loop"<<endl:cout<<"no loop"<<endl;
}
