#include <bits/stdc++.h>
using namespace std;
int board[100][100];
int visit[100];

//solution
int sol(int n,int src,int dst){
    //terminating condition
    if(src==dst)return 0;
    //execution of dfs
    visit[src]=true;
    int mini=INT_MAX;
    //llop call
    for(int i=0;i<n;i++){
        if(board[src][i]==1&&!visit[i])
        mini=min(mini,sol(n,i,dst)+1);
    }
    visit[src]=false;
    //return minimum path from source to destination
    return mini;
}
int main(){
    int n,m;
    cin>>n>>m;
    bool visit[n];
    memset(visit,false, sizeof(visit));
    //matrix input
    for(int i=0;i<m;i++){
        int x,y;
        cin>>x>>y;
        board[x-1][y-1]=1;
        board[y-1][x-1]=1;
    }
    //sourc and destination
    int src,dst;
    cin>>src>>dst;
    src--;
    dst--;
    cout<<sol(n,src,dst)<<endl;
}
