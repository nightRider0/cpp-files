//
// Created by Akhilesh on 19-Mar-17.
//
#include <bits/stdc++.h>
using  namespace std;

int sol(int amount,vector<int> vec){
   //dp intilize
    vector< int > dp(vec.size()+1);
    for(int i=0;i<vec.size();i++){
        vec.push_back(0);
    }
    //memset(dp,0,vec.size());

    //que set
    queue<int> q;
    q.push(amount);

    //sol loop
    while(!q.empty()){
        int tempamount=q.front(); cout<<tempamount<<"  "<<dp[tempamount]<<endl;
        q.pop();
        for(int i=0;i<vec.size();i++){
            if(tempamount-vec[i]==0) return dp[tempamount]+1;

            if ((tempamount-vec[i]>0)&&(!dp[tempamount-vec[i]])){

                q.push(tempamount-vec[i]);

                dp[tempamount-vec[i]] = dp[tempamount]+1;

            }
        }
    }
    return -1;
}

int main(){
    int n,amount;
    cin>>n>>amount;
    vector<int> vec(n);
    for(int i=0;i<n;i++){
        int a;
        cin>>a;
        vec.push_back(a);
    }
    cout<<sol(amount,vec);
}
