//
// Created by Akhilesh on 19-Mar-17.
//

//bug


#include <bits/stdc++.h>
using namespace std;
long dppath(long n,long m){
    vector<vector<long> > vec(n);
    for(long i=0;i<n;i++){
        for(long j=0;j<m;j++){
            vec[i].push_back(0);
        }

    }
    for(long i=0;i<n;i++){
        vec[0][i]=1;
    }
    for(long j=0;j<m;j++){
        vec[j][0]=1;
    }
    for(long i=1;i<n;i++){
        for(long j=1;j<m;j++)
            vec[i][j]=(vec[i-1][j]+vec[i][j-1])%1000000007;
    }
    return vec[n-1][n-1];
}

long sol(long i,long j,vector<vector<long> > vec ){
    if(i==0||j==0)return 1;
    if(!vec[i][j])vec[i][j]=sol(i-1,j,vec)+sol(i,j-1,vec);
    return vec[i][j];
}

long recpath(long n,long m){
    vector<vector<long> > vec(n);
    for(long i=0;i<n;i++){
        for(long j=0;j<m;j++){
            vec[i].push_back(0);
        }

    }
    vec[0][0]=0;
    return sol(n-1,m-1,vec);
}

int main(){
    long n,m;
    cin>>n>>m;
    for(int i=1;i<=n;i++){
        for(int j=1;j<=m;j++){
            cout<<i<<"  "<<j<<"  "<<dppath(i,j)<<endl;
            cout<<i<<"  "<<j<<"  "<<recpath(i,j)<<endl;
        }
    }
}
