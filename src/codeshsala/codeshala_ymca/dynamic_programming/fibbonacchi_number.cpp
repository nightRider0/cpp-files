//
// Created by Akhilesh on 19-Mar-17.
//
#include <bits/stdc++.h>
using namespace std;

int recfib(int n,int arr[]){
    if(n==0)return 0;
    if(arr[n]==0)
        arr[n] = recfib(n-1,arr)+recfib(n-2,arr);
    return arr[n];
}

int itrfib(int n,int arr[]){
    for(int i=2;i<=n;i++){
        arr[i]=arr[i-1]+arr[i-2];
    }
    return arr[n];
}
int main(){
    int n;
    cin>>n;
    int arr[n+1]={0};
    arr[1]=1;
    cout<<recfib(n,arr)<<endl;
    cout<<itrfib(n,arr);
}