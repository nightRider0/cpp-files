//
// Created by Akhilesh on 01-Apr-17.
//
#include <bits/stdc++.h>
using namespace std;
int main(){
    int n;
    cin>>n;
    vector <pair<long,int> > vec;
    for(int i=0;i<n;i++){
        long a;
        cin>>a;
        vec.push_back(make_pair(a,0));
    }
    int maxi=INT_MIN;
    vector<pair<long,int> > ::iterator it1,it2;
    for(it1=vec.begin();it1!=vec.end();it1++){
        for(it2=vec.begin();it2!=it1;it2++){
            if(it1->first>it2->first) {
                it1->second = max(it1->second, it2->second + 1);
            }
        }
        if(it1->second==0)it1->second=1;
        maxi=max(maxi,it1->second);
    }
    cout<<maxi<<endl;
}
