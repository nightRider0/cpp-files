#include<iostream>
using namespace std;
struct node {
int data;
node * l=0;
node * r=0;
};
node *smallest_value(node *root){
    if(!root)
    {
        return root;
    }
    while(true)
    {
        if(!root->l)
        {
            return root;
        }
        else
            root=root->l;
    }
}
node *second_smallest(node *root){
    if(!root)
    {
        return root;
    }
    if(!root->l)
    {
        if(root->r)
            return root->r;
        else
            return root;
    }
    while(true)
    {
        if(!root->l->l)
        {
            if(root->l->r)
            {
                return smallest_value(root->l->r);
            }
        }
        else
        {
            root=root->l;
        }
    }
}
traversal(node * root){
    if(!root){
        return;
    }
    traversal(root->l);
    traversal(root);
    traversal(root->r);
}
check_bst(node*root){
    traversal(root);
}
int main()
{
}
