//============================================================================
// Name        : level_order_traversal_again.cpp
// Author      : Akhilesh
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C, Ansi-style
//============================================================================

#include <iostream>
#include <algorithm>
#include <queue>
#include <utility>
using namespace std;
#define f_in(st) freopen(st,"r",stdin);
#define f_out(st) freopen(st,"w",stdout);
struct node {
	int data;
	struct node * left;
	struct node * right;
} * root = NULL;

struct node * insertNode(struct node * tNode, int value) {
	if(tNode == NULL) {
		struct node * newNode = (struct node *)malloc(sizeof(struct node));
		newNode->data = value;
		newNode->left = NULL;
		newNode->right = NULL;
		return newNode;
	}
	if(tNode->data > value)
		tNode->left = insertNode(tNode->left, value);
	else
		tNode->right = insertNode(tNode->right, value);
	return tNode;
}

void buildTree(int a[], int N) {
	for(int i = 0; i < N; i++) {
		if(i) {
			insertNode(root, a[i]);
		} else {
			root = insertNode(NULL, a[i]);
		}
	}
}

void levelOrderTraversal(struct node * tNode) {

			queue<pair<struct node*,char> > q;
    		pair<struct node*,char> p,tp;
    		p=make_pair(tNode,'e');
    		q.push(p);
    		struct node *temp;
    		while(!q.empty())
    		{
    			tp=q.front();
    			q.pop();
    			temp=tp.first;
    			if(!temp)
    			continue;
    			cout<<temp->data<<" "<<tp.second;
    			q.push(make_pair(temp->left,'n'));
    			if(tp.second=='e')
    			{
    				q.push(make_pair(temp->right,'e'));
    				cout<<endl;
    			}
    			else
    			q.push(make_pair(temp->right,'n'));
    		}
}

int main() {
	int N;
	cin >> N;
	int arr[N];
	for(int i = 0; i < N; i++) {
		cin >> arr[i];
	}
	buildTree(arr, N);
	levelOrderTraversal(root);
}



