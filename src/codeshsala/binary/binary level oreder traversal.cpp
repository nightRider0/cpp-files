#include<iostream>
#include<queue>
#include<vector>
using namespace std;
struct node {
int data;
node * l=0;
node * r=0;
};
void insert_node(node * root,int value)
{
    if(root->data<value)
    {
        if(!root->r)
        {
            node*temp=new node;
            temp->data=value;
            root->r=temp;
            return;
        }
        else
            insert_node(root->r,value);
    }
    else if(root->data>value)
    {
        if(!root->l)
        {
            node*temp=new node;
            temp->data=value;
            root->l=temp;
            return;
        }
        else
            insert_node(root->l,value);
    }
}
void bst(node * root,int value)
{
    if(!root)
    {
        root=new node;
        root->data=value;
        root->l=0;
        root->r=0;
        return;
    }
    else
    {
        insert_node(root,value);
        return;
    }
}
void bfs(node*root)
{
    node * temp;
    queue <node*> Q;
    Q.push(root);
    while(!Q.empty())
    {
        temp=Q.front();
        Q.pop();
        cout<<temp->data;
        if(temp->r)
        {
            Q.push(temp->r);
        }
        if(temp->l)
        {
            Q.push(temp->l);
        }

    }
    return;

}
void bfs_pyramid(node *root)
{
    node * temp;
    queue <node*> Q;
    Q.push(root);
    while(!Q.empty())
    {
        int xsize=Q.size();
        while(xsize--)
        {
            temp=Q.front();
            Q.pop();
            cout<<temp->data;
            if(temp->r)
            {
                Q.push(temp->r);
            }
            if(temp->l)
            {
                Q.push(temp->l);
            }
        }

    }
    return;
}
void levelorder_zigzag(node *root)
{
    node * temp;
    queue <node*> Q;
    Q.push(root);
    bool Key=true;
    while(!Q.empty())
    {
        vector<int>v;
        int xsize=Q.size();
        while(xsize--)
        {
            temp=Q.front();
            Q.pop();
                if(key)
                cout<<temp->data;
                else
                {
                    v.push_back(temp->data);

                }
            if(temp->r)
            {
                Q.push(temp->r);
            }
            if(temp->l)
            {
                Q.push(temp->l);
            }
        }
        if(!v.empty())
        {
            while(!v.empty())
                cout<<v.pop_back();
        }
        key=key?false:true;
        cout<<endl;

    }
    return;
}
void levelorder_reverse(node *root)
{
    node * temp;
    queue <node*> Q;
    Q.push(root);
    bool Key=true;
    queue<int>vf
    while(!Q.empty())
    {
        vector<int>v;
        int xsize=Q.size();
        while(xsize--)
        {
            temp=Q.front();
            Q.pop();
                if(key)
                vf.push(temp->data);
                else
                {
                    v.push_back(temp->data);

                }
            if(temp->r)
            {
                Q.push(temp->r);
            }
            if(temp->l)
            {
                Q.push(temp->l);
            }
        }
        if(!v.empty())
        {
            while(!v.empty())
                vf.push(v.pop_back());
        }
        key=key?false:true;
        cout<<endl;

    }
    while(vf.empty())
    {
        cout<<vf.front();
    }
    return;
}
