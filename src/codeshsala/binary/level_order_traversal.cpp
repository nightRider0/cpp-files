/*
 *
 *  Created on: Nov 19, 2016
 *      Author: Akhilesh Rao
 */
#include <iostream>
#include <algorithm>
#include <queue>
using namespace std;
#define f_in(st) freopen(st,"r",stdin);
#define f_out(st) freopen(st,"w",stdout);
struct node {
	int data;
	struct node * left;
	struct node * right;
} * root = NULL;

struct node * insertNode(struct node * tNode, int value) {
	if(tNode == NULL) {
		struct node * newNode = (struct node *)malloc(sizeof(struct node));
		newNode->data = value;
		newNode->left = NULL;
		newNode->right = NULL;
		return newNode;
	}
	if(tNode->data > value)
		tNode->left = insertNode(tNode->left, value);
	else
		tNode->right = insertNode(tNode->right, value);
	return tNode;
}

void buildTree(int a[], int N) {
	for(int i = 0; i < N; i++) {
		if(i) {
			insertNode(root, a[i]);
		} else {
			root = insertNode(NULL, a[i]);
		}
	}
}

void levelOrderTraversal(struct node * tNode) {

		queue<struct node*> q;
    		q.push(tNode);
    		struct node *temp;
    		while(!q.empty())
    		{
    			temp=q.front();
    			q.pop();
    			if(!temp)
    			continue;
    			cout<<temp->data;
    			q.push(temp->left);
    			q.push(temp->right);
    		}
}

int main() {
	int N;
	cin >> N;
	int arr[N];
	for(int i = 0; i < N; i++) {
		cin >> arr[i];
	}
	buildTree(arr, N);
	levelOrderTraversal(root);
}



