#include<iostream>
using namespace std;
struct node
{
    int data;
    node* next;
};
node * llist()
{
    int n;
    //cout<<"number of items you want to enter";
    cin>>n;
    node* start=0,*last=0,*temp=0;
    while(n--)
    {
        temp=new node;
        cin>>temp->data;
        if(start==0)
        {
        	temp->next=0;
            start=temp;
            last=temp;
        }
        else
        {
            temp->next=last;
            last=temp;
        }
    }
    return last;
}
int main()
{
    node *list1=llist(),*traveler;
    traveler=list1;
    while(traveler)
    {
    	cout<<traveler->data<<" ";
    	traveler=traveler->next;

    }
    cout<<endl;
}

