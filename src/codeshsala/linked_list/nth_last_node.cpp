//
// Created by akhilesh on 17/11/17.
//
#include<bits/stdc++.h>
using namespace std;

struct node {
    long int data;
    struct node*next;
};

node* input(int t){
    int num;
    node *ptr = nullptr, *head = nullptr;
    for(int i=0; i<t; i++) {
        cin>>num;
        auto * new_node = new node;
        new_node->data = num;
        if(head== nullptr)
            head = new_node;
        if(ptr)
            ptr->next=new_node;
        ptr = new_node;
    }
    if(ptr) ptr->next= nullptr;
    return head;
}

int main() {
    int t;
    cin>>t;
    node  *head1= input(t);
    int n;
    cin>> n;
    for(int i=0;i< t-n ;i++){
        head1 = head1->next;
    }
    cout<<head1->data<<endl;

}

