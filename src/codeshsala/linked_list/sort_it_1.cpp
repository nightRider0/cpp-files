//
// Created by Akhilesh on 14-Apr-17.
//
#include<bits/stdc++.h>
using namespace std;
struct node
{
   long long int data;
    node* next;
};
node * llist()
{
    long long int n;
    //cout<<"number of items you want to enter";
    cin>>n;
    node* start=0,*last=0,*temp=0;
    while(n--)
    {
        temp=new node;
        cin>>temp->data;
        temp->next=0;
        if(start==0)
        {
            start=temp;
            last=temp;
        }
        else
        {
            last->next=temp;
            last=last->next;
        }
    }
    return start;
}
node* merge_two(node* list1,node* list2){
    node * FinalList=0,*FinalListTravel=0;
    while(list1!=0&&list2!=0)
    {
        if(list1->data<=list2->data)
        {
            if(FinalList==0)
            {
                FinalList=list1;
                FinalListTravel=FinalList;
            }
            else
            {
                FinalListTravel->next=list1;
                FinalListTravel=FinalListTravel->next;
            }
            list1=list1->next;
        }
        else
        {
            if(FinalList==0)
            {
                FinalList=list2;
                FinalListTravel=FinalList;
            }
            else
            {
                FinalListTravel->next=list2;
                FinalListTravel=FinalListTravel->next;
            }
            list2=list2->next;
        }
    }
    if(list1)
    {
        FinalListTravel->next=list1;
    }
    else if(list2)
    {
        FinalListTravel->next=list2;
    }
    return FinalList;

}
node*solution(node* head)
{
    if(!head)
        return head;
    node *Pointer1,*Pointer2,*temp;
    Pointer1=head;
    Pointer2=head->next;
    while(true)
    {
        if(!Pointer2){
            return head;
        }
        if(Pointer2->data<Pointer1->data){
            Pointer1->next=0;
            return merge_two(head,Pointer2);
        }
        else{
            Pointer2=Pointer2->next;
            Pointer1=Pointer1->next;
        }
    }
}
void print (node* temp)
{
    while(temp)
    {
        cout<<temp->data<<" ";
        temp=temp->next;
    }
}
int main()
{
    node *head;
    head=llist();
    head=solution(head);
    print(head);
}