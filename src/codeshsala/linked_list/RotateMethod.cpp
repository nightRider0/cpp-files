//
// Created by Akhilesh on 14-Apr-17.
//
#include<bits/stdc++.h>
using namespace std;
struct node
{
    long long int data;
    node* next;
};
node * llist()
{
    long long int n;
    //cout<<"number of items you want to enter";
    cin>>n;
    node* start=0,*last=0,*temp=0;
    while(n--)
    {
        temp=new node;
        cin>>temp->data;
        temp->next=0;
        if(start==0)
        {
            start=temp;
            last=temp;
        }
        else
        {
            last->next=temp;
            last=last->next;
        }
    }
    return start;
}
int rotater(node * start,node* end){
   node* temp=start;
    long long temp1;
    do{

        //swap
        temp1=start->data;
        start->data=end->data;
        end->data=temp1;

        temp=start;
        while(temp->next!=end) {
            temp = temp->next;
        }
        end=temp;
        start=start->next;
    }while(start!=end&&end->next!=start);
        return -1;

}
int sol(node *list1,int rotate){
    node*p1=list1,*p4=list1;
    int temp;
    while(p4){
        temp=rotate-1;
        while(temp&&p4->next){
            p4=p4->next;
            temp--;
        }
        //cout<<rotater(p1,p4);//cout << " start->data " << p1->data << " end->data" << p4->data << " \n";
        rotater(p1,p4);
        p4=p1=p4->next;
    }
    return -2;

}
void print(node* temp){
    while(temp){
        cout<<temp->data<<" ";
        temp=temp->next;
    }
}
int main(){
    node *list1=llist();
    int rotate;
    cin>>rotate;
    //cout<<sol(list1,rotate);
    sol(list1,rotate);
    print(list1);
}
