#include<iostream>
using namespace std;
struct node
{
    long int data;
    node* next;
};
node * llist()
{
    long int n;
    //cout<<"number of items you want to enter";
    cin>>n;
    node* start=0,*last=0,*temp=0;
    while(n--)
    {
        temp=new node;
        cin>>temp->data;
        temp->next=0;
        if(start==0)
        {
            start=temp;
            last=temp;
        }
        else
        {
            last->next=temp;
            last=last->next;
        }
    }
    return start;
}
void xmerge(node *p1,node *p2)
{
    node *list1,*list2,*flist=0,*fltravel=0;
    list1=p1;
    list2=p2;
    while(list1!=0&&list2!=0)
    {
        if(list1->data<=list2->data)
        {
            if(flist==0)
            {
                flist=list1;
                fltravel=flist;
            }
            else
            {
                fltravel->next=list1;
                fltravel=fltravel->next;
            }
            list1=list1->next;
        }
        else
        {
            if(flist==0)
            {
                flist=list2;
                fltravel=flist;
            }
            else
            {
                fltravel->next=list2;
                fltravel=fltravel->next;
            }
            list2=list2->next;
        }
    }
    if(list1)
    {
        fltravel->next=list1;
    }
    else if(list2)
    {
        fltravel->next=list2;
    }
    fltravel=flist;
    while(fltravel)
    {
        cout<<fltravel->data<<" ";
        fltravel=fltravel->next;
    }
    cout<<endl;
}
void solution(node* head)
{
	node *p1,*p2,*temp;
	p1=p2=head;
	while(p2->data<p2->next->data)
	{
		p2=p2->next;
		if(!p2->next)
		{
			xmerge(p1,p2);
			return;
		}

	}
	p1=p2;
	p1->next=0;
	p2=p2->next;
	p1=head;
	xmerge(p1,p2);
	return;
}

int main()
{
	node *head;
	head=llist();
	solution(head);
}

