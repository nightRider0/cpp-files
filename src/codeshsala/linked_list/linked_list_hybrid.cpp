#include<iostream>

using namespace std;

struct node
{
    int value;
    node *next;
};

void add(node *ptr)
{
    node *temp=new node;
    cout<<"enter value for new node";
    cin>>temp->value;
    temp->next=NULL;
    if(ptr==NULL)
    {
        ptr=temp;
    }
    else
    {
        ptr->next=temp;
    }
}

void del(node * ptr)
{
    int value;
    cout<<"enter value you want to delete";
    cin>>value;
    if(ptr->value==value)
    {
        ptr=ptr->next;
        return;
    }
    while(true)
    {
        if(ptr->next->value==value)
        {
            ptr->next=ptr->next->next;
            break;
        }
        else if( ptr->next->next==NULL)
        {
            break;
        }
        ptr=ptr->next;
    }
}

void view(node * ptr)
{
    while(ptr)
    {
        cout<<ptr->value<<" ";
        ptr=ptr->next;
    }
}

void check(node *ptr)
{
    int value;
    cout<<"enter value you want to check";
    cin>>value;
    while(true)
    {
        if(ptr->value==value)
        {
            cout<<"\nfound your value\n";
            break;
        }
        else if(ptr->next==NULL)
        {
            cout<<"\n Sorry but your value does not exist in this list\n";
            break;
        }
    }
}

int main()
{
    node *start,*ptr;
    start=ptr=NULL;
    char again;
    do
    {
        int choice;
        cout<<"**************************************************\n\t1:-Add\n\t2:-Delete\n\t3:-View list\n\t4:-Check value"<<"\nenter your choice\n";
        cin>>choice;
        switch (choice)
        {
            case 1 :add(start);break;
            case 2 :del(start);break;
            case 3 :view(start);break;
            case 4 :check(start);break;
            default :cout<<"\nchoose wisely\n";

        }
        cout<<"\nwant another try\n";
        cin>>again;
    }
    while(again=='y'||again=='Y');
    return 0;
}
