#include<iostream>
using namespace std;
struct node
{
    long int data;
    node* next;
};
node * llist()
{
    long int n;
    //cout<<"number of items you want to enter";
    cin>>n;
    node* start=0,*last=0,*temp=0;
    while(n--)
    {
        temp=new node;
        cin>>temp->data;
        temp->next=0;
        if(start==0)
        {
            start=temp;
            last=temp;
        }
        else
        {
            last->next=temp;
            last=last->next;
        }
    }
    return start;
}
node * sortit(node * p1,node*p2)
{
    node head,*ptr;
    ptr=&head;
    while(p2&&p1)
    {

        if(p1->data>p2->data)
        {
            ptr->next=p2;
            p2=p2->next;
            ptr=ptr->next;
        }
        else if(p1->data<=p2->data)
        {
            ptr->next=p1;
            p1=p1->next;
            ptr=ptr->next;
        }
    }
    if(p2)
    {
        ptr->next=p2;
    }
    else if(p1)
    {
        ptr->next=p1;
    }
    return head.next;

}
node* divideit(node * temp)
{
    node *p2,*p3;
    p2=temp;
    p3=p2->next;
    while(p3)
    {
        if(p2->data>p3->data)
        {
            p2->next=0;
            return sortit(temp,p3);
        }
        else
        {
            p2=p2->next;
            p3=p3->next;
        }
    }
    return temp;
}
void print (node* temp)
{
	while(temp)
	{
		cout<<temp->data<<" ";
		temp=temp->next;
	}
}
int main()
{
	node *head;
	head=llist();
	head=divideit(head);
	print(head);
}

