#include<iostream>
#include<map>
using namespace std;
struct node
{
	node* previous;
    int data;
    node* next;
};
node* llist()
{
    int n;
    //cout<<"number of items you want to enter";
    cin>>n;
    node* start=0,*last=0,*temp=0;
    while(n--)
    {
        temp=new node;
        cin>>temp->data;
        if(start==0)
        {
        	temp->previous=temp;
        	temp->next=temp;
            start=temp;
            last=start;
        }
        else
        {
        	temp->previous=last;
        	temp->next=start;
            last->next=temp;
            start->previous=temp;
            last=last->next;
        }
    }

    return start;
}
node* solution(node*fwd,node*revr)
{
	int n;
	cin>>n;
	n--;
	while(n--)
	{
	    //cout<<revr->data<<endl;
		revr=revr->previous;
	}
	return revr;
}
node* ender(node*lst)
{
	node * temp;
    while(temp->next!=lst)
    {
        temp=temp->next;
    }
    return temp;
}
int main()
{
    node *list1st=0,*list1en=0;
    list1st=llist();

    list1en=list1st->previous;
    //cout<<"list1st list1st->previous"<<list1st<<"   "<<list1st->previous<<endl;
    list1en=solution(list1st,list1en);
    //cout<<"start end"<<list1st<<"  "<<list1en<<endl;
    list1st=list1en;
    cout<<list1en->data<<" ";
    list1en=list1en->next;
    while(list1en!=list1st)
    {
    	cout<<list1en->data<<" ";
    	list1en=list1en->next;
    }
    cout<<endl;
}
