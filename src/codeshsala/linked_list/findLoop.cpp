//
// Created by akhilesh on 17/11/17.
//
#include <stdio.h>
#include <stdlib.h>
struct node {
    int data;
    struct node * next;
};

// Implement this method only
bool isLoopExist(struct node * head) {
    node * temp1= head, *temp2 = head->next;
    while(temp2 && temp1){
        if(temp1 == temp2) return true;
        temp1 = temp1->next;
        temp2 = temp2->next;
        if(temp2) temp2=temp2->next;
    }
    return false;

}



// Do not modify below code.
void createLoop(int M, struct node * head) {
    int nodeNum = 1;
    if(M) {
        struct node * temp = head, * ramp = head;
        while(temp -> next) {
            temp = temp -> next;
        }
        while(ramp) {
            if(nodeNum == M)
                break;
            nodeNum++;
            ramp = ramp -> next;
        }
        temp -> next = ramp;
    }
}


struct node * createLinkedList(int N) {
    struct node * head = NULL, * prevNode = NULL;
    for(int i = 0; i < N; i++) {
        int value;
        scanf("%d", &value);
        struct node * temp = (struct node *)malloc(sizeof(struct node));
        temp -> data = value;
        temp -> next = NULL;

        if(!i) {
            head = temp;
        }
        else {
            prevNode -> next = temp;
        }
        prevNode = temp;
    }
    return head;
}


int main() {
    int N, M;
    scanf("%d", &N);
    struct node * head = createLinkedList(N);
    scanf("%d", &M);
    createLoop(M, head);
    if(isLoopExist(head))
        printf("YES\n");
    else
        printf("NO\n");
    return 0;
}
