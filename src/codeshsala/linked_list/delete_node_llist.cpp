#include<iostream>
using namespace std;
struct node
{
    int data;
    node* next;
};
node * llist()
{
    int n;
    //cout<<"number of items you want to enter";
    cin>>n;
    node* start=0,*last=0,*temp=0;
    while(n--)
    {
        temp=new node;
        cin>>temp->data;
        temp->next=0;
        if(start==0)
        {
            start=temp;
            last=temp;
        }
        else
        {
            last->next=temp;
            last=last->next;
        }
    }
    return start;
}
int main()
{
    node *list1=llist(),*traveler;
    int value,index;
    traveler=list1;
    cin>>index;
    if(index!=1)
    {
        index-=2;
        while(index--)
        {
    	    traveler=traveler->next;
        }
        traveler->next=traveler->next->next;
    }
    else
    {
        list1=list1->next;
   	    traveler=list1;
    }
    traveler=list1;
    while(traveler)
    {
    	cout<<traveler->data<<" ";
    	traveler=traveler->next;
    }
    cout<<endl;
}
