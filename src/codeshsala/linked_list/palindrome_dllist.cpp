#include<iostream>
#include<map>
using namespace std;
struct node
{
	node* previous;
    char data;
    node* next;
};
node* llist()
{
    int n;
    //cout<<"number of items you want to enter";
    cin>>n;
    node* start=0,*last=0,*temp=0;
    while(n--)
    {
        temp=new node;
        cin>>temp->data;
        temp->next=0;
        if(start==0)
        {
        	temp->previous=0;
            start=temp;
            last=temp;
        }
        else
        {
        	temp->previous=last;
            last->next=temp;
            last=last->next;
        }
    }

    return start;
}
string solution(node*fwd,node*revr)
{
	while(fwd!=revr)
	{
	    //cout<<"fwd,back"<<fwd<<"  "<<revr<<" "<<fwd->data<<" "<<revr->data<<endl;
		if(fwd->data==revr->data)
		{
			fwd=fwd->next;
			revr=revr->previous;
		}
		else
		{
			return "NO\n";
		}
	}
	return "YES\n";

}
node* ender(node*lst)
{
    while(lst->next)
    {
        lst=lst->next;
    }
    return lst;
}
int main()
{
    node *list1st=0,*list1en=0;
    list1st=llist();
    list1en=ender(list1st);
   // cout<<"start end"<<list1st<<"  "<<list1en<<endl;
    cout<<solution(list1st,list1en);
}
