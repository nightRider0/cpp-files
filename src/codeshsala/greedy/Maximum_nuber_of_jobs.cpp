//
// Created by Akhilesh Rao on 29-Jan-17.
//
#include <bits/stdc++.h>
//#include <utility>
using namespace std;
int main(){
    vector<pair<int,int> > vec;
    int n;
    cin>>n;
    for(int i=0;i<n;i++){
        int j,k;
        cin>>j>>k;
        vec.push_back(make_pair(k,j));
    }
    sort(vec.begin(),vec.end());

    int count=0,previousrecod=0;

    for(int i = 0; i < vec.size(); i++) {
        if(vec[i].second>=previousrecod){
            //cout<<"vec"<<vec[i].second<<"  "<<previousrecod<<endl;
            count++;
            previousrecod=vec[i].first;
        }

    }
    cout<<count;
    return 0;
}
