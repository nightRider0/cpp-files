//
// Created by akhilesh on 17/11/17.
//
#include<bits/stdc++.h>
using namespace std;

struct node {
    long int data;
    struct node*next;
};

node* input(){
    int t;
    cin>>t;
    int num;
    node *ptr = nullptr, *head = nullptr;
    while(t--) {
        cin>>num;
        auto * new_node = new node;
        new_node->data = num;
        if(head== nullptr)
            head = new_node;
        if(ptr)
            ptr->next=new_node;
        ptr = new_node;
    }
    if(ptr) ptr->next= nullptr;
    return head;
}

int main() {
    node  *head1= input(),*head2=input();
    while(head1 && head2){
        if(head1->data == head2 ->data ){
            cout << head1->data << endl;
            head1 = head1->next;
            head2 = head2->next;
        }else if (head1->data > head2->data){
            head2 = head2->next;
        }else{
            head1 = head1->next;
        }
    }

    if(!(head1 && head2)){
        cout << -1 << endl;
    }


}
