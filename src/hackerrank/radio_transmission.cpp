//
// Created by Akhilesh on 22-08-2017.
//
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int n;
    int k;
    cin >> n >> k;
    vector<int> x(n);
    for (int x_i = 0; x_i < n; x_i++) {
        cin >> x[x_i];
    }
    sort(x.begin(), x.end());

    bool tower = false;
    int anchor = 0;
    int sol = 0;
    for (int i = 0; i < x.size();) {
        anchor = x[i];
        while (x[i] <= anchor + k) {
            i++;
            if (i > x.size())break;
        }
        i--;
        tower = true;
        anchor = x[i];
        sol++;
        while (x[i] <= anchor + k) {
            i++;
            if (i > x.size())break;
        }
        tower = false;
    }
    cout << sol << endl;
}

