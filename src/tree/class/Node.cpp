//
// Created by Akhilesh Rao on 21-Dec-16.
//

#include "Node.h"

node::node(int data = 0 ,node* left = 0, node*right = 0) {
        this->__data = data;
        this->__left = left;
        this->__right = right;
    }

void node::set_right(node* temp){
    this->__right=temp;
}

void node::set_left(node*temp){
    this->__left=temp;
}

void node::set_data(int data) {
    this->__data = data;
}

int node::get_data(void) {
        return this->__data;
    }

node*node::get_right(void) {
        return this->__right;
    }

node*node::get_left(void) {
        return this->__left;
    }

