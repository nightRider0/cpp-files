//
// Created by Akhilesh Rao on 21-Dec-16.
//

#ifndef TREE_NODE_H
#define TREE_NODE_H
class node{
private:
    int    __data;
    node * __left;
    node * __right;
public:
    node(int ,node*,node*);

    void set_right(node*);

    void set_left(node*);

    void set_data(int );

    int get_data(void);

    node* get_left(void);

    node* get_right(void);
};
#endif //TREE_NODE_H
