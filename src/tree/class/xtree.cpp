//
// Created by Akhilesh Rao on 21-Dec-16.
//
#include <bits/stdc++.h>
#include "xtree.h"
using namespace std;

xtree::xtree() {}

xtree::xtree(int data) {
    this->__root=new node(data,0,0);
}

void xtree::append(int data) {
    if(this->__root == 0) {
        this->__root = new node(data,0,0);
        return;
    }
    node* temp;
    temp = this->__root;
    while(true) {
        if(temp->get_data() < data){
            if(temp->get_right() == 0){
                temp->set_right(new node(data, 0, 0));
                break;
            }
            temp= temp->get_right();
        }
        else if(temp->get_data()>data){
            if(temp->get_left() == 0){
                temp->set_left(new node(data, 0, 0));
                break;
            }
            temp= temp->get_left();
        }else{
            break;
        }
    }
}

void xtree::append(vector <int> arr){
    for (int it : arr){
        this->append(it);
    }
}

void xtree::bfs(void){
    queue<node*> q;
    q.push(this->__root);
    while(!q.empty()){
        node * temp;
        temp = q.front();
        q.pop();
        cout << temp->get_data() <<" ";
        if(temp->get_left()){
            q.push(temp->get_left());
        };
        if(temp->get_right()){
            q.push(temp->get_right());
        };
    }
    cout<<endl;
}

void xtree::print(void) {
    node * xtr = new node(0,0,0);
    node * temp ;
    queue <node *>q;
    q.push(this->__root);
    q.push(0);
    while(true){
        temp = q.front();
        q.pop();
        if(temp == 0){
            if(q.empty()){
                break;
            }
            cout<<endl;
            q.push(0);
        }else if(temp == xtr){
            cout<< " - ";
        }else{
            cout<<" "<< temp->get_data()<<" ";
            if(temp->get_left() == 0){
                q.push(xtr);
            } else{
                q.push(temp->get_left());
            }
            if(temp->get_right() == 0){
                q.push(xtr);
            } else{
                q.push(temp->get_right());
            }
        }
    }

}

void xtree::traversalin(node*temp){
    if(temp == 0){
        return;
    }
    traversalin(temp->get_left());
    cout<< temp->get_data() <<" ";
    traversalin(temp->get_right() );
}

void xtree::traversalpre(node*temp){
    if(!temp){
        return;
    }
    cout<< temp->get_data()<<" ";
    traversalpre(temp->get_left() );
    traversalpre(temp->get_right() );
}

void xtree::traversalpos(node*temp){
    if(!temp){
        return;
    }
    traversalpos(temp->get_left() );
    traversalpos(temp->get_right() );
    cout<< temp->get_data()<<" ";
}

void xtree::inorder(void){
    traversalin(this->__root);
    cout<<endl;
}

void xtree::preorder(void){
    traversalpre(this->__root);
    cout<<endl;
}

void xtree::postorder(void){
    traversalpos(this->__root);
    cout<<endl;
}

void xtree::search_node(node * &parent , node * &child , int value){
    if (this->__root == 0){
        parent = child = 0;
        return;
    }
    node * temp = this->__root;
    if(temp->get_data() == value){
        parent = 0;
        child = temp;
        return;
    }else if (temp->get_data()<value){
        if(temp->get_right()) child = temp->get_right();
        else {
            parent = child = 0;
            return;
        }
    }else {
        if(temp->get_left()) child = temp->get_left();
        else {
            parent = child = 0;
            return;
        }
    }

    while (true){
        if(child->get_data() == value){
            return;
        }else if(child->get_data()<value){
            if(child->get_right()){
                parent = child;
                child = child->get_right();
            }else{
                parent = child =0;
                return;
            }
        }else{
            if(child->get_left()){
                parent = child;
                child = child->get_left();
            }else{
                parent = child =0;
                return;
            }
        }
    }

}

node * xtree::l_r_most(node * root){
    if(root ->get_left()) root = root->get_left();
    else return root;
    while(true){
        if(root->get_right()) root =root->get_right();
        else return root;
    }
}

node * xtree::r_l_most(node * root){
    if(root->get_right()) root =root->get_right();
    else return root;
    while(true){
        if(root->get_left()) root = root->get_left();
        else return root;
    }
}

void xtree::delete_value(int data){
    node * parent, * child;
    this->search_node(parent , child, data);
    if(!(parent || child)){
        cout<<"Value not found"<<endl;
        return;
    }else if (!parent){
        if(child->get_right()&&child->get_left()){
            node * t = this->l_r_most(child);
            child->set_data(t->get_data());
            delete t ;
        }else if (child->get_left()){
            node * t = this->l_r_most(child);
            child->set_data(t->get_data());
            delete t ;
        }else if(child->get_right()){
            node * t = this->r_l_most(child);
            child->set_data(t->get_data());
            delete t;
        }else{
            delete child;
            this->__root = 0;
        }
    }else{
        if(child->get_right()&&child->get_left()){
            node * t = this->l_r_most(child);
            child->set_data(t->get_data());
            delete t ;
        }else if (child->get_left()){
            node * t = this->l_r_most(child);
            child->set_data(t->get_data());
            delete t ;
        }else if(child->get_right()){
            node * t = this->r_l_most(child);
            child->set_data(t->get_data());
            delete t;
        }else{
            if (parent->get_left() == child){
                parent->set_left(0);
                delete child;
            }else if(parent->get_right() == child){
                parent->set_left(0);
                delete child;
            }else{
                cout<<"Something gone wrong\n";
            }
        }
    }

}