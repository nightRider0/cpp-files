//
// Created by Akhilesh Rao on 21-Dec-16.
//
#include "Node.h"
#include <vector>
#ifndef TREE_XTREE_H
#define TREE_XTREE_H

class xtree
{
private:
    node * __root = 0;
    void traversalin(node*);
    void traversalpre(node*);
    void traversalpos(node*);
    void search_node(node* &, node* &, int);
    node * r_l_most(node *);
    node* l_r_most(node *);
public:
    xtree(void);
    xtree(int);
    void append(int);
    void append(std::vector<int>);
    void delete_value(int);
    void bfs(void);
    void print(void);
    void inorder(void);
    void preorder(void);
    void postorder(void);

};
#endif //TREE_XTREE_H

