//
// Created by Akhilesh on 03-10-2017.
//
#include <bits/stdc++.h>

using namespace std;

int possibleways(vector < int > &input1)
{
    int sol = 0;
    int i = 1;
    long long sum = accumulate(input1.begin(), input1.end(),0);
    for( ;i <= input1.size(); i++){
        sol += (input1[i-1] * input1[i-1] * i)%1000000007;
        sol += (i*input1[i-1]*(sum-input1[i-1]))%1000000007;
        sum -= input1[i-1];
    }
    return sol;
}


int main() {
    int output;
    int ip1_size = 0;
    cin >> ip1_size;
    //cin.ignore (std::numeric_limits<std::streamsize>::max(), '\n');
    vector<int> ip1;
    int ip1_item;
    for(int ip1_i=0; ip1_i<ip1_size; ip1_i++) {
        cin >> ip1_item;
        //cin.ignore (std::numeric_limits<std::streamsize>::max(), '\n');
        ip1.push_back(ip1_item);
    }
    for(auto it: ip1 ){cout<<it<<endl;}
    output = possibleways(ip1);
    cout << output << endl;
    return 0;
}
