//
// Created by Akhilesh on 18-Apr-17.
//
#include <bits/stdc++.h>

using namespace std;

class Super
{ int x;
public:
    void funBase() { cout << "Super function\n"; }
};

class Sub : public Super
{
    int y;
};

int main()
{
    Super* ptr;    // Super class pointer
    Sub obj;
    ptr = &obj;

    Super &ref=obj;    // Super class's reference
}
