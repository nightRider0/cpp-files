//
// Created by Akhilesh on 18-Apr-17.
//
#include <bits/stdc++.h>
using namespace std;
class ForwardReference
{
    int i;
public:
    ForwardReference():i(0){}
    int f() {return g()+10;}  // call to undeclared function
    int g() {return i;}
};

int main()
{
    ForwardReference fr;
    cout<<fr.f();
}
