//
// Created by Akhilesh on 18-Apr-17.
//
#include<bits/stdc++.h>

using namespace std;

class parent{
public:
    parent(){
        cout<<"I am parent constructor\n";
    }
    ~parent(){
        cout<<"parent destructor\n";
    }
};

class daughter: private parent{
public:
    daughter(){
        cout<<"Hey I am daughter Constructor\n";
    }
    ~daughter(){
        cout<<"Daughter destructor\n";
    }
};
int main(){

}

/*class distance{
private:
    int a , b;
public:
    void get_data(){
        std::cout<<"enter a and b\n";
        std::cin>>a>>b;
    }
    void show_data(){
        std::cout<<"a :"<<a<<std::endl;
        std::cout<<"b :"<<b<<std::endl;
    }
    void add(distance d1,distance d2){
        a=d1.a+d2.a;
        b=d1.b+d2.b;
    }
};
int main() {
    daughter d1;
    delete(d1);
    //distance d1;
    //distance d2;
    //distance d3;
    //d1.get_data();
    //d2.get_data();
    //d3.add(d1,d2);
    //d3.show_data();
}*/