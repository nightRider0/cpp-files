//
// Created by Akhilesh on 19-Apr-17.
//
#include <bits/stdc++.h>

using namespace std;

class test{
    bool value;
    int number;
public:
    test(bool x=true,int y=0):value(x),number(y){}

    void show_value(){
        cout<<value<<" "<<number<<endl;
    }
    test operator ! (){
        value=!value;
        return test(value);
    }
    int operator + (){
        number++;
        return number;
    }
    int operator ++ (int){
        number++;
        return number;
    }
};

int main() {
    test t1(false);
    !t1;
    t1.show_value();
    !t1;
    t1.show_value();
    +t1;
    t1.show_value();
    t1++;
    t1.show_value();
}
