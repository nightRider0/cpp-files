//
// Created by Akhilesh on 19-Apr-17.
//
#include <bits/stdc++.h>

using namespace std;
class SafeArray{
private:
    int arr[10];
public:
    SafeArray(){
        for(int i=0;i<10;i++){
            arr[i]=i;
        }
    }

    int& operator []=(int i) {
        if(i>10){
            cout<<"Array out of bound\n";
            return arr[0];
        }
        return arr[i];
    }

};


int main() {
    SafeArray s;
    cout<<s[1]<<endl;
    cout<<s[9]<<endl;
    cout<<s[10]<<endl;
}