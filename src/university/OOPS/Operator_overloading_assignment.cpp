//
// Created by Akhilesh on 19-Apr-17.
//
#include <iostream>
using namespace std;

class Distance {
private:
    int feet;             // 0 to infinite
    int inches;           // 0 to 12
public:
    // required constructors
    Distance(){
        feet = 0;
        inches = 0;
    }

    Distance(int f, int i){
        feet = f;
        inches = i;
    }

    void operator = (const Distance & d){
        feet=d.feet;
        inches=d.inches;
    }
    // method to display distance
    void displayDistance() {
        cout << "F: " << feet <<  " I:" <<  inches << endl;
    }
    //friend bool operator== (Distance , Distance);
    bool operator== (Distance &d){
        return(d.feet==feet&&d.inches==inches);

    }
};




int main() {
    Distance D1(11, 10), D2(5, 11);

    cout << "First Distance : ";
    D1.displayDistance();
    cout << "Second Distance :";
    D2.displayDistance();

    // use assignment operator
    D1 = D2;
    cout << "First Distance :";
    D1.displayDistance();
    cout<<(D1==D2)<<endl;

    return 0;
}
