//
// Created by Akhilesh on 24-Apr-17.
//
#include <bits/stdc++.h>

using namespace std;

class A
{
protected:
    int a;
public:
    void getdata(){
        cin>>a;
    }
    void showdata()
    {
        cout<<a;
    }
    A operator ++(int)
    {
        a++;
    }
};
class B:public A
{
public:
    A operator--(int)
    {
        a--;
    }
};
int main() {
    B obj;
    obj.getdata();
    obj.showdata();
    obj++;
    obj.showdata();
    obj--;
    obj.showdata();
    
}
