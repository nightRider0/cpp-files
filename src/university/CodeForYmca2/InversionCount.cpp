//
// Created by Akhilesh on 23-Apr-17.
//
#include <bits/stdc++.h>
using namespace std;
int investionCount=0;
void merge(int arr[],int left,int right,int mid){
    int temp[right-left+1];
    int i=left;
    int j=mid;
    int k=0;
    while(i<=mid-1&&j<=right){
        if(arr[i]>arr[j]){
            investionCount+=mid-i;
            temp[k]=arr[j];
            j++;k++;
        }
        else{
            temp[k]=arr[i];
            i++;k++;
        }
    }
    while(i<mid){
        temp[k]=arr[i];
        i++;k++;
    }
    while(j<=right){
        j++;k++;
    }
    for(int i=left;i<=right;i++){
        arr[i]=temp[i];
    }
}
void mergeSort(int arr[],int left,int right){
    if(right>left) {
        int mid = (left + right) / 2;
        mergeSort(arr, left, mid-1);
        mergeSort(arr, mid , right);
        merge(arr, left, right, mid);
    }
}

int main()
{
    int n;
    cin>>n;
    int arr[n];
    for(int i=0;i<n;i++){
        cin>>arr[i];
    }
    mergeSort(arr,0,n-1);
    cout<<investionCount<<endl;
    return 0;
}
