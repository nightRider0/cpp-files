//
// Created by Akhilesh on 23-Apr-17.
//
#include <bits/stdc++.h>

using namespace std;


int main() {
    int n;
    cin >> n;
    while (n--) {
        int arr[10];
        memset(arr,0, sizeof(arr));
        long long i;
        cin>>i;
        if(i<0)i*=(-1);
        while(i){
            arr[i%10]++;
            i/=10;
        }
        int k;
        cin>>k;
        cout<<arr[k]<<endl;
    }
}
