//
// Created by Akhilesh on 23-Apr-17.
//
#include <bits/stdc++.h>

using namespace std;

void addto(vector<vector<int> > &Matrix,int a,int b){
    Matrix[a].push_back(b);
    return;
}

bool Find(vector<vector<int> > &Matrix,int a ,int b){
    queue<int>q;
    for(int i=0;i<Matrix[a].size();i++){
        q.push(Matrix[a][i]);
    }
    while(!q.empty()){
        int temp=q.front();
        q.pop();
        if(temp==b){
            return true;
        }
        for(int i=0;i<Matrix[temp].size();i++){
            q.push(Matrix[temp][i]);
        }
    }
    return false;
}

int main() {
    int t;
    cin>>t;
    vector <vector<int> > Matrix(100003);
    int typ,a,b;
    while(t--){
        cin>>typ>>a>>b;
        if(typ==1){
            addto(Matrix,a,b);
        }
        else{
            if(Find(Matrix,a,b)){
                cout<<"Yes\n";
            }
            else {
                cout << "No\n";
            }
        }
    }
}
