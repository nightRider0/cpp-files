//
// Created by Akhilesh on 09-08-2017.
//
#include <bits/stdc++.h>
#include <backward/strstream>

using namespace std;

vector<string> split(string const &line){
    strstream  buffer;
    buffer<<line;
    vector<string > vec;
    while(buffer) {
        string temp;
        buffer>>temp;
        if(temp.empty())continue;
        vec.push_back(temp);
    }
    return vec;
}

int main(){
    map <string, vector<string> > index;
    char datafile[][10] = { ("A:\\f1.txt"),("A\\f2.txt")};
    vector < string > xclude={"is", "the", "on", "of", "has", "have", "are", "will", "shall"};
    for(int i =0 ;i< sizeof(datafile)/(10* sizeof('a')) ;i++){
        ifstream file;
        file.open(datafile[i]);
        if(!file){
            cout<<"file not opend";
            return -1;
        }
        string line;
        while(getline(file, line)){
            cout<<line<<endl;
            vector < string > vec = split(line);
            for(string &it : vec){
                if( find(xclude.begin(),xclude.end(),it) == xclude.end()){
                    index[it].emplace_back(datafile[i]);
                }
            }
            for(string &it : vec ){
                cout<< it << ": -- ";
                for(string &value : index[it]){
                    cout<< value<<", ";
                }
                cout<<endl;
            }
        }
        file.close();
    }

    ofstream file("A:\\index.txt",ios::out);
    file<<'{'<<"\r\n";
    for(pair<string,vector<string> > it : index ){
        file<< it.first << ": ";
        for(string &value : index[it.first]){
            file << value << ", ";
        }
        file<<"\r\n";
    }
    file<<'}'<<"\r\n";
    file.close();
}
