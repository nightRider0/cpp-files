//
// Created by Akhilesh on 04-11-2017.
//
#include<bits/stdc++.h>

using namespace std;

class player{
private:
    bool alive = true;
    int life = 100;
    int weapon = 2;
public:

    bool isAlive()  {
        return player::alive;
    }

    void setAlive(bool alive) {
        player::alive = alive;
    }

    int getLife()  {
        return life;
    }

    void setLife(int life) {
        player::life = life;
        if(player::life <1){
            player::setAlive(false);
        }
    }

    int getWeapon()  {
        return weapon;
    }

    void setWeapon(int weapon) {
        player::weapon = weapon;
    }

    void shoot(){
        if(! player::alive ){
            cout<<"Player is already dead\n";
            return;
        }
        player::life--;
    }
};


int main() {
    player p1, p2, p3;
    while (true){
        cout<<"enter player to hit";
        int t ;
        cin >> t;
        switch (t) {
            case 1:
                p1.shoot();
                cout<< "player 1 life "<<p1.getLife()<< "\n";
                break;
            case 2:
                p2.shoot();
                cout<< "player 2 life "<<p2.getLife()<< endl;
                break;
            case 3:
                p3.shoot();
                cout<< "player 3 life "<<p3.getLife()<< endl;
                break;
            default:
                cout<<"invalid choice \n";
        }
    }
}
