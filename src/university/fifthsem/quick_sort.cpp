//
// Created by Akhilesh on 22-08-2017.
//
#include <bits/stdc++.h>
using namespace std;
void print(int arr[],int n){
    for(int i=0;i<n;i++){
        cout<<arr[i]<<" ";
    }
    cout<<endl;
}
void swap (int *a , int *b){
    int temp = *a;
    *a=*b;
    *b=temp;
}
int partition(int arr[], int start, int end){
        int i=start -1;
        int pivot = arr[end];
        for(int j=start ; j<end-1;j++){
            if(arr[j] <= pivot) {
                i++;
                swap(&arr[i], &arr[j]);
            }
        }
        swap(&arr[i+1],&arr[end]);
        return i+1;

}
void quick_sort(int arr[], int start, int end){
    if(start<end){
        cout<<"::" <<start<< " " <<end<<endl;
        print(arr,end+1);
        int pi = partition(arr, start, end);
        quick_sort(arr,start,pi-1);
        quick_sort(arr,pi+1,end);

    }

}
int main(){
    int n;
    cin>>n;
    int arr[n];
    for(int i=0 ; i < n; i++){
        cin>>arr[i];
    }
    quick_sort(arr,0,n-1);
    print(arr,n);
}
