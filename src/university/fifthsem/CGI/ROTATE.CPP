#include<iostream.h>
#include<conio.h>
#include<stdlib.h>
#include<graphics.h>
#include<math.h>
int main(void) {
    int gd = DETECT, gm;
    initgraph(&gd, &gm, "C:\\TC\\BGI");
    int x1, y1, x2, y2, x3, y3, x4, y4;
    cout << "Enter the four coordinates of rectangle: ";
    cin >> x1 >> y1 >> x2 >> y2 >> x3 >> y3 >> x4 >> y4;
    line(x1, y1, x2, y2);
    line(x2, y2, x3, y3);
    line(x3, y3, x4, y4);
    line(x4, y4, x1, y1);
    int theta, tx, ty;
    cout << "Enter angle of rotation: ";
    cin >> theta;
    cout << "Enter translation vectors: ";
    cin >> tx >> ty;
    int a1, b1, a2, b2, a3, b3, a4, b4;
    a1 = (x1 * (cos(theta))) - (y1 * (sin(theta))) + tx;
    b1 = (x1 * (sin(theta))) + (y1 * (cos(theta))) + ty;
    a2 = (x2 * (cos(theta))) - (y2 * (sin(theta))) + tx;
    b2 = (x2 * (sin(theta))) + (y2 * (cos(theta))) + ty;
    a3 = (x3 * (cos(theta))) - (y3 * (sin(theta))) + tx;
    b3 = (x3 * (sin(theta))) + (y3 * (cos(theta))) + ty;
    a4 = (x4 * (cos(theta))) - (y4 * (sin(theta))) + tx;
    b4 = (x4 * (sin(theta))) + (y4 * (cos(theta))) + ty;
    line(a1, b1, a2, b2);
    line(a2, b2, a3, b3);
    line(a3, b3, a4, b4);
    line(a4, b4, a1, b1);
    getch();
    closegraph();
    return 0;
}