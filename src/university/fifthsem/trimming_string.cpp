//
// Created by Akhilesh on 02-08-2017.
//
#include <bits/stdc++.h>
using namespace std;
int main(){
    string s;
    cin>>s;
    cout<<"prefixes : \n";
    cout<<'$'<<endl;
    for(unsigned long i = s.length() ; i>0;i--){
        for(int j=0;j< i; j++){
            cout<<s[j];
        }
        cout<<endl;
    }
    cout<<"Proper prefix : \n";
    for(unsigned long i = s.length()-1; i>0;i--){
        for(int j=0;j< i; j++){
            cout<<s[j];
        }
        cout<<endl;
    }
    cout<<"Suffix :\n";
    cout<<'$'<<endl;
    for(unsigned long i = 0; i< s.length();i++){
        for(unsigned long j=i;j< s.length(); j++){
            cout<<s[j];
        }
        cout<<endl;
    }
    cout<<"Proper Suffix :\n";
    for(unsigned long i = 1; i< s.length();i++){
        for(int j=i;j< s.length(); j++){
            cout<<s[j];
        }
        cout<<endl;
    }

    cout<<"substring : \n";
    for(int start =0 ; start < s.length(); start++){
        for(unsigned long end = s.length(); end > start ; end-- ){
            for (int i =start; i!= end;  i++){
                cout<<s[i];
            }
            cout<<endl;
        }
    }
}
