//
// Created by Akhilesh on 09-10-2017.
//
#include <bits/stdc++.h>
using namespace std;

int main(){
    random_device rd;
    default_random_engine generator( rd() );
    uniform_int_distribution<int> distribution(0 , 199);
    auto random_value = bind(distribution, generator);
    int pos = random_value();
    cout<<"initial position of head : "<< pos<<endl;
    vector<int> vec;
    int total_head_movement = 0;
    int cont = random_value();
    cout<<"Number of movements = "<< cont <<endl;
    for(int i=0;i<cont; i++){
        vec.push_back(random_value());
    }

    while(!vec.empty()){
        int nearest = INT_MAX;
        int next = 0;
        vector<int>::iterator itr;
        auto element = vec.begin();
        for(; element != vec.end(); ++element){
            if(min(200-abs(*element-pos),abs(*element-pos)) < nearest ){
                next = *element;
                nearest = abs(*element-pos);
                itr = element;
            }
        }
        cout<< pos << "->" << next <<endl;
        total_head_movement+=(abs(pos-next));
        vec.erase(itr);
        pos = next;
    }
    cout<<"total head movement = " << total_head_movement << endl;
}
