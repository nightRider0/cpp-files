//
// Created by Akhilesh on 09-10-2017.
//
#include <bits/stdc++.h>
using namespace std;

int main(){
    random_device rd;
    default_random_engine generator( rd() );
    uniform_int_distribution<int> distribution(0 , 199);
    auto random_value = bind(distribution, generator);
    int pos = random_value();
    cout<<"initial position of head : "<< pos<<endl;
    vector<int> vec;
    int total_head_movement = 0;
    int cont = random_value();
    cout<<"Number of movements = "<< cont <<endl;
    for(int i=0;i<cont; i++){
        vec.push_back(random_value());
    }
    sort( vec.begin(), vec.end() );
    vec.erase( unique( vec.begin(), vec.end() ), vec.end() );
    cont = (int)vec.size();
    cout<<"Number of movements = "<< cont <<endl;
    auto pivot = vec.begin();
    while(*pivot< pos){
        pivot++;
    }
    cout<<" Pivot Point = "<< *pivot <<endl;

    while(pivot!=vec.end()) {
        cout << pos << "->" << *pivot << endl;
        total_head_movement += (abs(*pivot - pos));
        pos = *pivot;
        vec.erase(pivot);
    }
    --pivot;
    while(pivot!=vec.begin()){
        cout<< pos << "->" << *pivot <<endl;
        total_head_movement += (abs(*pivot-pos));
        pos = *pivot;
        vec.erase(pivot);
        --pivot;
    }
    cout<<"total head movement = " << total_head_movement << endl;
}

