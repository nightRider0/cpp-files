//
// Created by Akhilesh on 09-10-2017.
//
#include <bits/stdc++.h>
using namespace std;
int main(){
    random_device rd;
    default_random_engine generator( rd() );
    uniform_int_distribution<int> distribution(0, 199);
    auto random_value = bind(distribution, generator);
    int pos = random_value();
    cout<<"initial position of head : "<< pos<<endl;
    int cont = random_value();
    cout<<"Number of movements = "<< cont <<endl;
    int total_head_movement = 0;
    cout<<"movements"<<endl;
    while(cont--){
        int next = random_value();
        cout<<pos << "->" << next<<endl;
        total_head_movement+=( abs(next-pos) );
        pos=next;
    }
    cout<<"total head movement = " << total_head_movement<<endl;
}