//
// Created by Akhilesh on 04-09-2017.
//
#include<bits/stdc++.h>
using namespace std;
//return a randow value between 3 and 100
/*int random_value(){
    time_t second;
    time(&second);
    srand((unsigned int)second);
    int t = INT_MAX;
    while( t <3 || t>100){
        t= (int)rand()%100;
    }
    return t;
}
*/

int main(){
    random_device rd;
    default_random_engine generator(rd());
    uniform_int_distribution<int> distribution(3,100);
    // generates number in the range 1..6
    auto random_value = std::bind ( distribution, generator );
    int jobs = random_value();
    jobs = 10;
    int arr[100]; //hash to track arrival time
    memset(arr,0, sizeof(arr));
    cout << "Number of jobs : " << jobs << endl;
    vector< pair< string, vector<int > * > > jobmap;  //main structure to hold all data

    string s ="job"; //job initial

    for(int i= 0;i <jobs; i++){
        int arrival_time = random_value();
        while(arr[arrival_time]) arrival_time = random_value();
        arr[arrival_time] = 1;

        int execution_time = random_value();

        auto *temp = new vector<int> ;
        temp->emplace_back(arrival_time);
        temp->emplace_back(execution_time);
        jobmap.emplace_back(make_pair(s+" "+to_string(i), temp));
    }

    sort(jobmap.begin(), jobmap.end(), [](const auto & lhs, const auto & rhs){
        if(lhs.second->at(1) < rhs.second->at(1)){
            return true;
        }else if(lhs.second->at(1) > rhs.second->at(1)){
            return false;
        }else{
            return lhs.second->at(0) < rhs.second->at(0);
        }
    });

    int master_time = jobmap[0].second->at(0);

    for(auto ptr : jobmap){
        int arrivaltime = ptr.second->at(0);
        int executiontime = ptr.second->at(1);
        int wating_time = max(0,master_time-arrivaltime);
        master_time += executiontime;

        int turn_aroundtime = wating_time + executiontime;
        ptr.second->emplace_back(wating_time);
        ptr.second->emplace_back(turn_aroundtime);
    }
    cout<< "jobs" << setw(25) << "arrival_time" << setw(20) << " execution time" << setw(20) << " waiting time"
        << setw(20) << " turnaroundtime " << setw(20) <<"normalised turnaround time \n";
    for (auto ptr : jobmap){
        cout<< ptr.first << setw(20) <<ptr.second->at(0) << setw(20) << ptr.second->at(1) << setw(20) << ptr.second->at(2) << setw(20)
            << ptr.second->at(3) << setw(20) << (float)ptr.second->at(3)/(float)ptr.second->at(1) <<endl;
    }
}
