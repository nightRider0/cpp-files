//
// Created by Akhilesh on 23-10-2017.
//
#include<bits/stdc++.h>

using namespace std;


class algo{
private:
    int size;
    deque<int> q;
    map<int, int> index ;

    bool check(int x){
        return find(q.begin(), q.end(), x)!=q.end() ;
    }

    void delboy(int x){
        for(deque<int>::iterator it= q.begin(); it!=q.end(); it++ ){
            if(*it == x){
                q.erase(it);
                break;
            }
        }
    }

public:

    explicit algo(int size) : size(size) {}

    bool insert(int x){
        if(q.size() == size) {
            int mini = INT_MAX, value= INT_MAX;
            if(check(x)){
                index[x]--;
                if(index[x]<0){index[x]=0;}
            }else{
                for(auto it : q){
                    if(mini < index[it] ){
                        value = it;
                        mini = index[it];
                    }
                    if(mini == 0 ){
                        break;
                    }
                }
                delboy(value);
                index[x]--;
                if(index[x]<0)index[x]=0;
                q.push_back(x);
            }
        }
        q.push_back(x);
        return true;
    }
    bool getMem(){
        for(auto i: q){
            cout<< i <<" ";
        }
        cout<<endl;
    }

    bool indexing(vector<int> vec){
        for(auto it: vec){
            index[it]++;
        }
        return true;
    }
};


int main() {

    random_device rd;
    default_random_engine generator(rd());
    uniform_int_distribution<int> distribution(0,9);
    auto random_value = bind(distribution, generator);

    //entry sequence;
    int size = random_value();
    cout<< "input size " << size<<endl;
    vector< int> vec;
    for(int i=0;i< size; i++){
        vec.emplace_back(random_value());
        cout<<vec[i]<<" ";
    }

    algo a(4);
    a.indexing(vec);
    for(auto it : vec){
        a.insert(it);
        a.getMem();
    }

}