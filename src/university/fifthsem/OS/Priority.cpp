//
// Created by Akhilesh on 08-10-2017.
//
#include<bits/stdc++.h>
using namespace std;

struct Process
{
    int pid;  // Process ID
    int bt;   // CPU Burst time required
    int priority; // Priority of this process
};

bool comparison(Process a, Process b) {
    return (a.priority > b.priority);
}

void findWaitingTime(Process proc[], int n, int wt[]) {
    wt[0] = 0;
    for (int  i = 1; i < n ; i++ )
        wt[i] =  proc[i-1].bt + wt[i-1] ;
}
void findTurnAroundTime( Process proc[], int n, int wt[], int tat[]){
    for (int  i = 0; i < n ; i++)
        tat[i] = proc[i].bt + wt[i];
}
void findavgTime(Process proc[], int n) {
    int wt[n], tat[n], total_wt = 0, total_tat = 0;
    findWaitingTime(proc, n, wt);
    findTurnAroundTime(proc, n, wt, tat);

    cout<< "\njobs"  << setw(20) << " execution time" << setw(20) << " waiting time"
        << setw(20) << " turnaroundtime " << setw(40) <<"normalised turnaround time \n";
    for (int i=0; i<n; i++) {
        total_wt = total_wt + wt[i];
        total_tat = total_tat + tat[i];
        cout << "job"+to_string(proc[i].pid)  <<setw(15) << proc[i].bt
             <<setw(20) << wt[i] <<setw(20) << tat[i]  <<setw(25) << (float)tat[i]/(float)proc[i].bt <<endl;
    }

    cout << "\nAverage waiting time = "
         << (float)total_wt / (float)n;
    cout << "\nAverage turn around time = "
         << (float)total_tat / (float)n;
}

void priorityScheduling(Process proc[], int n) {
    sort(proc, proc + n, comparison);
    cout<< "Order in which processes gets executed \n";
    for (int  i = 0 ; i <  n; i++)
        cout << proc[i].pid <<" " ;
    findavgTime(proc, n);
}
int main()
{
    Process proc[] = {{1, 10, 2}, {2, 5, 0}, {3, 8, 1}};
    int n = sizeof proc / sizeof proc[0];
    priorityScheduling(proc, n);
    return 0;
}
