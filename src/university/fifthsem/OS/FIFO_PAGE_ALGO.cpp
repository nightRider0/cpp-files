//
// Created by Akhilesh on 23-10-2017.
//
#include<bits/stdc++.h>
using namespace std;
class algo{
private:
    int size;
    deque<int> q;
public:
    explicit algo(int size) : size(size) {}
    bool insert(int x){
        if(q.size() == size) {
            if((find(q.begin(), q.end(), x) == q.end()) ){
                q.pop_front();
                q.push_back(x);
                return  true;
            }else{
                return false;
            }
        }
        q.push_back(x);
        return true;
    }
    bool getMem(){
        for(int i=0;i< q.size(); i++){
            cout<< q[i] <<" ";
        }
        cout<<endl;
    }
};
int main(){
    random_device rd;
    default_random_engine generator(rd());
    uniform_int_distribution<int> distribution(0,9);
    auto random_value = bind(distribution, generator);
    int size = random_value();
    cout<< "input size " << size<<endl;
    vector< int> vec;
    for(int i=0;i< size; i++){
        vec.emplace_back(random_value());
        cout<<vec[i]<<" ";
    }
    cout<<endl;
    algo a(4);
    for(auto i: vec){
        a.insert(i);
        a.getMem();
    }
}
