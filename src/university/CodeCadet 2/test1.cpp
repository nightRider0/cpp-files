//
// Created by Akhilesh on 23-Apr-17.
//
#include <bits/stdc++.h>

using namespace std;

bool isPrime(int n){
    for(int i=2;i<n;i++){
        if(n%i==0)return false;
    }
    return true;
}

int main() {
    int i=1;
    int n=2;
    while(i<9501){
        if(isPrime(n)){
            cout<<i<< " th is :"<< n<<endl;
            i++;
        }
        n++;
    }
}
