//
// Created by Akhilesh on 23-Apr-17.
//
#include <bits/stdc++.h>

using namespace std;

void increment(vector<int> &vec){
    int carry=1;
    for(int i=0;i<vec.size();i++){
        vec[i]=vec[i]+carry;
        carry=vec[i]/10;
        vec[i]=vec[i]%10;
        if(!carry)return;
    }
    if(carry)vec.push_back(carry);
    return;
}

long xsum(long n) {
    long sum=0;
    while(n){
        sum+=n%10;
        n=n/10;
    }
    return sum;
}

long total_Sum(long n){
    long long sum=0;
    while(n){
        sum+=xsum(n);
    }
    return sum;
}

int main() {
    for (long i = 0; i < 1000000000; i++) {
        long m ,  n = i;
        m=n;
        vector<int> vec;
        vec.push_back(1);
        int result = 0;
        while (n--) {
            result += accumulate(vec.begin(), vec.end(), 0);
            increment(vec);
        }
        cout <<"i is "<< i<< "  First is: " << result << " Second is: " << total_Sum(m) << endl;
    }
}
