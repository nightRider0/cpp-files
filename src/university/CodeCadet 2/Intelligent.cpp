//
// Created by Akhilesh on 23-Apr-17.
//
#include <bits/stdc++.h>

using namespace std;

class Intelligent{
private:
    vector<int> marks;
    int size;
    double sd;
    double mean;
public:
    Intelligent(){
        cin>>size;
        for (int i=0;i<size;i++){
            int a;
            cin>>a;
            marks.push_back(a);
        }
        mean=accumulate(marks.begin(),marks.end(),0.0);
        mean/=size;
        long sigma=0;
        for(int i=0;i<size;i++){
            sigma+=(marks[i]-mean)*(marks[i]-mean);
        }
        sd=sigma/size;
    }
    double getSigma(){
        return sd;
    }
};

int main() {
    Intelligent Namit,Aalok;
    if(Namit.getSigma()==Aalok.getSigma())cout<<"They Both are equally intelligent"<<endl;
    else if(Namit.getSigma()>Aalok.getSigma())cout<<"Namit"<<endl;
    else cout<<"Alok\n";
    return 0;
}