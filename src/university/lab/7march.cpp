//
// Created by Akhilesh on 07-Mar-17.
//
#include <bits/stdc++.h>
using namespace std;
class student{
private:
    string __name;
    int __rollnumber;
    int __marks;
    char __grade;


public:
    void grade_calculate(){
        if(__marks>=80)__grade='A';
        else if(__marks>=70)__grade='B';
        else if(__marks>=60)__grade='C';
        else __grade='D';
        cout<<"grade calculated  "<<__grade<<endl;
    }
    void read_data(){
        string name;
        int rollnumber;
        int marks;
        char grade;
        cout<<"name"<<endl;
        cin>>name;
        cout<<"rollnumber"<<endl;
        cin>>rollnumber;
        cout<<"marks"<<endl;
        cin>>marks;
        cout<<"at grade calculation\n";
        __name=name;
        __marks=marks;
        __rollnumber=rollnumber;

        grade_calculate();
    }
    void display_grade(){
        cout<<__name;
        printf("%10c",' ');
        cout<<__rollnumber;
        printf("%10c",' ');
        cout<<__marks;
        printf("%10c",' ');
        cout<<__grade<<endl;
    }
};

int main(){
    int i;
    cin>>i;
    student s[i];
    for(int j=0;j<i;j++){
        s[i].read_data();
    }
    cout<<"Name";
    printf("%10c",' ');
    cout<<"Rollnumber";
    printf("%10c",' ');
    cout<<"Mar";
    printf("%10d",' ');
    cout<<"Grade\n";
    for(int j=0;j<i;j++){
        s[i].display_grade();
    }

}
