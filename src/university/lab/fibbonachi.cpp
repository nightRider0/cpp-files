#include<iostream>
using namespace std;
long long int fibo(int value,long long int*dp){
    if(value==1||value==2){
        return 1;
    }
    if(dp[value]!=0)
        return dp[value];
    dp[value]=fibo(value-1,dp)+fibo(value-2,dp);
    return dp[value];
}
int main(){
    long long int dp[1000];
    for(int i=0;i<1000;i++){
        dp[i]=0;
    }
    while(true){
        int a;
        cin>>a;
        if(a==0)continue;
        cout<<a<<" th fibonachin is: "<<fibo(a,dp)<<endl;
    }
}
